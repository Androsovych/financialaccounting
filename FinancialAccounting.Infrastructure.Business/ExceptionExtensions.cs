﻿using System;
namespace FinancialAccounting.Infrastructure.Business
{
    public static class ExceptionExtensions
    {
        public static T ThrowIfNull<T>(this T argument, string argumentName)
        {
            if (argument == null)
            {
                throw new ArgumentNullException(argumentName);
            }

            return argument;
        }

        public static string ThrowIfNullOrEmptyOrWhitespaces(this string argument, string argumentName)
        {
            if (String.IsNullOrEmpty(argument) || String.IsNullOrWhiteSpace(argument))
            {
                throw new ArgumentException(argumentName);
            }

            return argument;
        }
    }
}
