﻿namespace FinancialAccounting.Infrastructure.Business
{
    public class JwtGeneratorConfig
    {
        public const string Issuer = "Jwt:Issuer";
        public const string Audience = "Jwt:Audience";
        public const string Key = "Jwt:Key";
        public const string LifeTime = "Jwt:LifeTime";
    }
}
