﻿using FinancialAccounting.Services.Interfaces;
using FinancialAccounting.Domain.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using System.Security.Claims;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using FinancialAccounting.Infrastructure.Data;

namespace FinancialAccounting.Infrastructure.Business
{
    public class JwtGenerator : IJwtGenerator
    {
        private readonly IConfiguration _configuration;

        public JwtGenerator(IConfiguration config)
        {
            _configuration = config;
        }

        public string CreateToken(User user)
        {
            user = user.ThrowIfNull(nameof(user));
            user.UserName = user.UserName
                .ThrowIfNullOrEmptyOrWhitespaces(nameof(user.UserName));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration[JwtGeneratorConfig.Key]));

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.UserName)
            };

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _configuration[JwtGeneratorConfig.Issuer],
                    audience: _configuration[JwtGeneratorConfig.Audience],
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(Double.Parse(_configuration[JwtGeneratorConfig.LifeTime])),
                    signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
