﻿using FinancialAccounting.Services.Interfaces;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace FinancialAccounting.Infrastructure.Business
{
    public sealed class PasswordHasher : IPasswordHasher
    {
        const int SaltSize = 16;
        const int KeySize = 32;
        const char HashSeparator = '.';
        const int HashElementsNum = 3;

        readonly KeyDerivationPrf KeyDerivationPrf = KeyDerivationPrf.HMACSHA256;

        HashingOptions _options;

        public PasswordHasher(IOptions<HashingOptions> options)
        {
            _options = options.Value;
        }

        public (bool Verified, bool NeedsUpgrade) Check(string hash, string password)
        {
            hash.ThrowIfNullOrEmptyOrWhitespaces(nameof(hash));
            password.ThrowIfNullOrEmptyOrWhitespaces(nameof(password));

            var parts = hash.Split(HashSeparator, HashElementsNum);

            if (parts.Length != HashElementsNum)
                throw new FormatException("Unexpected hash format. " +
                    "Should be formatted as `{iterations}.{salt}.{hash}`");

            var iterations = Convert.ToInt32(parts[0]);
            var salt = Convert.FromBase64String(parts[1]);
            var key = Convert.FromBase64String(parts[2]);

            var needsUpgrade = iterations != _options.Iterations;

            var keyToCheck = KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf,
                iterationCount: iterations,
                numBytesRequested: KeySize);

            var verified = keyToCheck.SequenceEqual(key);

            return (verified, needsUpgrade);
        }

        public string Hash(string password)
        {
            password.ThrowIfNullOrEmptyOrWhitespaces(nameof(password));

            byte[] salt = new byte[SaltSize];

            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetNonZeroBytes(salt);
            }

            string key = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: password,
            salt: salt,
            prf: KeyDerivationPrf,
            iterationCount: _options.Iterations,
            numBytesRequested: KeySize));

            var saltStr = Convert.ToBase64String(salt);

            return $"{_options.Iterations}.{saltStr}.{key}";
        }
    }
}