﻿namespace FinancialAccounting.Infrastructure.Business
{
    public sealed class HashingOptions
    {
        public int Iterations { get; set; } = 100;
    }
}