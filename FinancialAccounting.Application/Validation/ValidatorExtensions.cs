﻿using FinancialAccounting.Infrastructure.Data;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace FinancialAccounting.Application.Extensions
{
    public static class ValidatorExtensions
    {
		const char DecimalSeparator = '.';
		const char ZeroChar = '0';
		const string PropertyName = "{PropertyName}";
		static readonly List<char> charsToRemove = new List<char>(){ '-', ' ' };

        public static IRuleBuilder<T, string> Name<T>(this IRuleBuilder<T, string> ruleBuilder)
		{
			var options = ruleBuilder.NameLength()
				.Must(BeAValidName).WithMessage(Resources.Resources.InvalidNameCharacters);

			return options;
		}

		public static IRuleBuilder<T, string> NameLength<T>(this IRuleBuilder<T, string> ruleBuilder)
		{
			var options = ruleBuilder.Required()
				.Length(Configuration.MinNameLength, Configuration.MaxNameLength).WithMessage(
				String.Format(Resources.Resources.InvalidNameLength, PropertyName, Configuration.MinNameLength,
				Configuration.MaxNameLength));

			return options;
		}

		public static IRuleBuilder<T, string> Required<T>(this IRuleBuilder<T, string> ruleBuilder)
		{
			var options = ruleBuilder
				.NotNull().WithMessage(Resources.Resources.PropertyRequired);

			return options;
		}

		public static IRuleBuilder<T, decimal> AmountValue<T>(this IRuleBuilder<T, decimal> ruleBuilder)
		{
			var options = ruleBuilder.Must(BeAValidAmount).WithMessage(
				String.Format(Resources.Resources.InvalidAmount, Configuration.AmountWholePartLength,
				Configuration.AmountFractionalPartLength));

			return options;
		}

		public static IRuleBuilder<T, int> IdValue<T>(this IRuleBuilder<T, int> ruleBuilder)
		{
			var options = ruleBuilder.Must((i) => i > 0)
				.WithMessage(Resources.Resources.InvalidPositiveProperty);

			return options;
		}

		public static IRuleBuilder<T, DateTime> DateValue<T>(this IRuleBuilder<T, DateTime> ruleBuilder)
		{
			var options = ruleBuilder.Must((i) => i.Date <= DateTime.Today)
				.WithMessage(Resources.Resources.InvalidDate);

			return options;
		}

		private static bool BeAValidName(string name)
        {
            charsToRemove.ForEach(c => name = name.Replace(c.ToString(), String.Empty));

			return name.All(Char.IsLetter);
        }

		private static bool BeAValidAmount(decimal amount)
		{
			if (amount <= 0)
				return false;

			var parts = amount.ToString(CultureInfo.InvariantCulture).Split(DecimalSeparator);

			if (parts.Length < 2)
				return parts[0].Length <= Configuration.AmountWholePartLength;

			return parts[0].Length <= Configuration.AmountWholePartLength
				&& parts[1].TrimEnd(ZeroChar).Length <= Configuration.AmountFractionalPartLength;
		}
	}
}
