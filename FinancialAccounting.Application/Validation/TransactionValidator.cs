﻿using FinancialAccounting.Application.Extensions;
using FinancialAccounting.Application.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application
{
    public class TransactionValidator : AbstractValidator<TransactionWithoutIdDto>
    {
        public TransactionValidator()
        {
            RuleFor(x => x.WalletId).IdValue();
            RuleFor(x => x.Date).Must((i) => i.Date <= DateTime.Today).WithMessage(Resources.Resources.InvalidDate);
            RuleFor(x => x.Category).Cascade(CascadeMode.Stop).NameLength();
            RuleFor(x => x.Type).IsInEnum().WithMessage(Resources.Resources.InvalidTransactionType);
            RuleFor(x => x.Amount).AmountValue();
        }
    }
}
