﻿using System;
using System.Net;

namespace FinancialAccounting.Application.Exceptions
{
    public class RequestHandlingException : Exception
    {
        public HttpStatusCode Code { get; }
        public object Errors { get; }

        public RequestHandlingException(HttpStatusCode code, object errors = null)
        {
            Code = code;
            Errors = errors;
        }
    }
}
