﻿using FinancialAccounting.Application.Services;
using FinancialAccounting.Application.Services.Interfaces;
using FinancialAccounting.Infrastructure.Business;
using FinancialAccounting.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace FinancialAccounting.Application
{
    public static class ServiceCollectionExtensions
    {
        const string HashingIterations = "HashingIterations";

        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IJwtGenerator, JwtGenerator>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<ITransactionService, TransactionsService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IPasswordHasher, PasswordHasher>();

            services.Configure<HashingOptions>(options =>
            {
                options.Iterations = Int32.Parse(configuration[HashingIterations]);
            });

            return services;
        }
    }
}