﻿using FinancialAccounting.Domain.Core;

namespace FinancialAccounting.Application.Models
{
    public class WalletOutcomeDto
    {
        public int WalletId { get; set; }
        public string WalletName { get; set; }
        public decimal Total { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public Currency Currency { get; set; }

        public WalletOutcomeDto(Wallet wallet)
        {
            WalletName = wallet.Name;
            Currency = wallet.Currency;
            WalletId = wallet.Id;
        }

        public WalletOutcomeDto()
        {
        }
    }
}