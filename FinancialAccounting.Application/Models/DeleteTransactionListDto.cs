﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Models
{
    public class DeleteTransactionListDto
    {
        public int DeletedTrasactionsNumber { get; set; }
    }
}
