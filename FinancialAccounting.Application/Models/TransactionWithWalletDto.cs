﻿using FinancialAccounting.Domain.Core;

namespace FinancialAccounting.Application.Models
{
    public class TransactionWithWalletDto : TransactionDto
    {
        public string WalletName { get; set; }
        public Currency Currency { get; set; }

        public TransactionWithWalletDto(Transaction transaction) :
            base(transaction)
        {
            WalletName = transaction.Wallet.Name;
            Currency = transaction.Wallet.Currency;
        }
    }
}