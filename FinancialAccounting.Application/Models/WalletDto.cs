﻿using FinancialAccounting.Domain.Core;
using System.ComponentModel.DataAnnotations;

namespace FinancialAccounting.Application
{
    public class WalletDto
    {
        const string InvalidCurrency = "InvalidCurrency";

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = InvalidCurrency,
            ErrorMessageResourceType = typeof(Resources.Resources))]
        public Currency? Currency { get; set; }

        public WalletDto(Wallet wallet)
        {
            Id = wallet.Id;
            UserId = wallet.UserId;
            Name = wallet.Name;
            Currency = wallet.Currency;
        }
    }
}