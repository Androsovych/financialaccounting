﻿using FinancialAccounting.Domain.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application
{
    public class UserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }

        public UserDto(User user)
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            UserName = user.UserName;
        }
    }
}
