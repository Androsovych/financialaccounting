﻿using FinancialAccounting.Domain.Core;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace FinancialAccounting.Application.Models
{
    public class TransactionWithoutIdDto : BaseRequest
    {
        const string InvalidTransactionType = "InvalidTransactionType";

        public int WalletId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Comment { get; set; }

        [Required(ErrorMessageResourceName = InvalidTransactionType, ErrorMessageResourceType = typeof(Resources.Resources))]
        public TransactionType? Type { get; set; }
        public string Category { get; set; }

        public TransactionWithoutIdDto(Transaction transaction)
        {
            Date = transaction.Date;
            Amount = transaction.Amount;
            Type = transaction.Type;
            Comment = transaction.Comment;
            Category = transaction.Category.Name;
            WalletId = transaction.WalletId;
        }

        public TransactionWithoutIdDto()
        {
        }
    }
}