﻿using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Domain.Core;

namespace FinancialAccounting.Application.Models
{
    public class WalletOutcomeWithTransactionsDto : WalletOutcomeDto
    {
        public PagedList<TransactionDto> Transactions { get; set; }

        public WalletOutcomeWithTransactionsDto(Wallet wallet) : base(wallet)
        {
        }

        public WalletOutcomeWithTransactionsDto(WalletOutcomeDto wallet)
        {
            WalletName = wallet.WalletName;
            Currency = wallet.Currency;
            WalletId = wallet.WalletId;
            Credit = wallet.Credit;
            Debit = wallet.Debit;
            Total = wallet.Total;
        }
    }
}