﻿using FinancialAccounting.Domain.Core;
using System;
using System.Text.Json.Serialization;

namespace FinancialAccounting.Application.Models
{
    public class TransactionDto : TransactionWithoutIdDto
    {
        public int Id { get; set; }
        public TransactionDto(Transaction transaction):
            base(transaction)
        {
            Id = transaction.Id;
        }

        public TransactionDto()
        {
        }
    }
}