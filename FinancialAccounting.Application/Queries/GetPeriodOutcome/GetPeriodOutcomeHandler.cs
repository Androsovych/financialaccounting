﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries
{
    public class GetPeriodOutcomeHandler : IRequestHandler<GetPeriodOutcomeQuery, GetPeriodOutcomeResponse>
    {
        IUnitOfWork _unitOfWork;
        IWalletService _walletService;

        public GetPeriodOutcomeHandler(IUnitOfWork unitOfWork, IWalletService walletService)
        {
            _unitOfWork = unitOfWork;
            _walletService = walletService;
        }

        public async Task<GetPeriodOutcomeResponse> Handle(GetPeriodOutcomeQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<Transaction, bool>> predicate;

            if (request.StartDate.Date == request.EndDate.Date)
                predicate = (t) => t.Date.Date == request.StartDate.Date;
            else
                predicate = (t) => t.Date.Date >= request.StartDate.Date && t.Date.Date <= request.EndDate.Date;

            var user = await _unitOfWork.Users.GetWithTransactionsAsync(request.UserName, predicate);

            if (user == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

            return new GetPeriodOutcomeResponse()
            {
                UserName = user.UserName,
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                Wallets = _walletService.ConvertToWalletOutcomes(user.Wallets)
            };
        }
    }
}