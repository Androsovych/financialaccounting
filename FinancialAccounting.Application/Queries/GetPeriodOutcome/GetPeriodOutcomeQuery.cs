﻿using MediatR;
using System;

namespace FinancialAccounting.Application.Queries
{
    public class GetPeriodOutcomeQuery : BaseRequest, IRequest<GetPeriodOutcomeResponse>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}