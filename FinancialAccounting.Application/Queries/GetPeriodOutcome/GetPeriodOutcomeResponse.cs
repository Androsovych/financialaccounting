﻿using FinancialAccounting.Application.Models;
using System;

namespace FinancialAccounting.Application.Queries
{
    public class GetPeriodOutcomeResponse : GetOutcomeResponse
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}