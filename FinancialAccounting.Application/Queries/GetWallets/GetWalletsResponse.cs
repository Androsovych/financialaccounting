﻿using FinancialAccounting.Application.Helpers;

namespace FinancialAccounting.Application.Queries
{
    public class GetWalletsResponse : BaseRequest
    {
        public PagedList<WalletDto> Wallets { get; set; }
    }
}