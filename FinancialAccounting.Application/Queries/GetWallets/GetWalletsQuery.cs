﻿using FinancialAccounting.Application.Models;
using MediatR;

namespace FinancialAccounting.Application.Queries
{
    public class GetWalletsQuery : BaseRequest, IRequest<GetWalletsResponse>
    {
        public WalletParameters WalletParameters { get; set; }
    }
}
