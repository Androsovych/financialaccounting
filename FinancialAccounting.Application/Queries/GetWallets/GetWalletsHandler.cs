﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries
{
    public class GetWalletsHandler : IRequestHandler<GetWalletsQuery, GetWalletsResponse>
    {
        IUnitOfWork _unitOfWork;
        IWalletService _walletService;

        public GetWalletsHandler(IUnitOfWork unitOfWork, IWalletService walletService)
        {
            _unitOfWork = unitOfWork;
            _walletService = walletService;
        }

        public async Task<GetWalletsResponse> Handle(GetWalletsQuery request, CancellationToken cancellationToken)
        {
            var wallets = await _unitOfWork.Wallets.FindAsync(w => w.User.UserName == request.UserName);

            if (wallets == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

           _walletService.ConvertWallets(wallets);

            return new GetWalletsResponse
            {
                UserName = request.UserName,
                Wallets = PagedList<WalletDto>.ToPagedList(_walletService.ConvertWallets(wallets),
                    request.WalletParameters.PageNumber, request.WalletParameters.PageSize)
            };
        }
    }
}