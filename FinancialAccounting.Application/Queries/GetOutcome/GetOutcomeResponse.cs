﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Models
{
    public class GetOutcomeResponse : BaseRequest
    {
        public List<WalletOutcomeDto> Wallets { get; set; } = new List<WalletOutcomeDto>();
    }
}
