﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Queries;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Handlers
{
    public class GetOutcomeHandler : IRequestHandler<GetOutcomeQuery, GetOutcomeResponse>
    {
        IUnitOfWork _unitOfWork;
        IWalletService _walletService;

        public GetOutcomeHandler(IUnitOfWork unitOfWork, IWalletService walletService)
        {
            _unitOfWork = unitOfWork;
            _walletService = walletService;
        }

        public async Task<GetOutcomeResponse> Handle(GetOutcomeQuery request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Users.GetWithTransactionsAsync(request.UserName);

            if (user == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

            return new GetOutcomeResponse()
            {
                UserName = user.UserName,
                Wallets = _walletService.ConvertToWalletOutcomes(user.Wallets)
            };
        }
    }
}