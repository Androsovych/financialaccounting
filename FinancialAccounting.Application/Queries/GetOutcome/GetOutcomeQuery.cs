﻿using FinancialAccounting.Application.Models;
using MediatR;

namespace FinancialAccounting.Application.Queries
{
    public class GetOutcomeQuery : BaseRequest, IRequest<GetOutcomeResponse>
    {
    }
}