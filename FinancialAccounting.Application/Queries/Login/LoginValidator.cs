﻿using FinancialAccounting.Application.Extensions;
using FinancialAccounting.Application.Queries;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Validators
{
    public class LoginValidator : AbstractValidator<LoginQuery>
	{
		public LoginValidator()
		{
			RuleFor(x => x.UserName).NameLength();
			RuleFor(x => x.Password).Required();
		}
    }
}
