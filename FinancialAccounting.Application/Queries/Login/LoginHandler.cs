﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Queries;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using FinancialAccounting.Services.Interfaces;
using MediatR;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application
{
    public class LoginHandler : IRequestHandler<LoginQuery, UserDto>
    {
        IUnitOfWork _unitOfWork;
        IJwtGenerator _jwtGenerator;
        IPasswordHasher _passwordHasher;

        public LoginHandler(IUnitOfWork unitOfWork, IJwtGenerator jwtGenerator, IPasswordHasher passwordHasher)
        {
            _unitOfWork = unitOfWork;
            _jwtGenerator = jwtGenerator;
            _passwordHasher = passwordHasher;
        }

        public async Task<UserDto> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
			var user = await _unitOfWork.Users.GetSingleAsync(u => u.UserName == request.UserName);

            if (user == null)
				throw new RequestHandlingException(HttpStatusCode.NotFound);

            var result = _passwordHasher.Check(user.Password, request.Password);

            if (!result.Verified)
                throw new RequestHandlingException(HttpStatusCode.Unauthorized);

            if (result.NeedsUpgrade)
                await UpgradeHash(user, request.Password);

			return new UserDto(user)
			{
				Token = _jwtGenerator.CreateToken(user)
			};
		}

        private async Task UpgradeHash(User user, string password)
        {
            user.Password = _passwordHasher.Hash(password);
            _unitOfWork.Users.Update(user);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}
