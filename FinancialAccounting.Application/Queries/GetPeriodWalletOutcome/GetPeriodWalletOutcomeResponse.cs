﻿using FinancialAccounting.Application.Models;
using System;

namespace FinancialAccounting.Application.Queries.GetPeriodWalletOutcome
{
    public class GetPeriodWalletOutcomeResponse : BaseRequest
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public WalletOutcomeWithTransactionsDto WalletOutcome { get; set; }
    }
}
