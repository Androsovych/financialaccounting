﻿using FinancialAccounting.Application.Models;
using MediatR;
using System;

namespace FinancialAccounting.Application.Queries.GetPeriodWalletOutcome
{
    public class GetPeriodWalletOutcomeQuery : BaseRequest, IRequest<GetPeriodWalletOutcomeResponse>
    {
        public int WalletId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TransactionParameters TransactionParameters { get; set; }
    }
}