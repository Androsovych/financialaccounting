﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries.GetPeriodWalletOutcome
{
    public class GetPeriodWalletOutcomeAuthorizer : AbstractRequestAuthorizer<GetPeriodWalletOutcomeQuery>
    {
        public override void BuildPolicy(GetPeriodWalletOutcomeQuery request)
        {
            UseRequirement(new MustOwnWalletRequirement
            {
                WalletId = request.WalletId,
                UserName = request.UserName
            });
        }
    }
}
