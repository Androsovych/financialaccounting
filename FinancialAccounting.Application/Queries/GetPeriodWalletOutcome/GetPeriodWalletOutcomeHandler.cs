﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries.GetPeriodWalletOutcome
{
    public class GetPeriodWalletOutcomeHandler : IRequestHandler<GetPeriodWalletOutcomeQuery, GetPeriodWalletOutcomeResponse>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionService;
        IWalletService _walletService;

        public GetPeriodWalletOutcomeHandler(IUnitOfWork unitOfWork, ITransactionService transactionService, IWalletService walletService)
        {
            _unitOfWork = unitOfWork;
            _transactionService = transactionService;
            _walletService = walletService;
        }

        public async Task<GetPeriodWalletOutcomeResponse> Handle(GetPeriodWalletOutcomeQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<Transaction, bool>> predicate;

            if (request.StartDate.Date == request.EndDate.Date)
                predicate = (t) => t.Date.Date == request.StartDate.Date;
            else
                predicate = (t) => t.Date.Date >= request.StartDate.Date && t.Date.Date <= request.EndDate.Date;

            var wallet = await _unitOfWork.Wallets.GetWithTransactionsAsync(request.WalletId, predicate);

            if (wallet == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

            return new GetPeriodWalletOutcomeResponse
            {
                UserName = request.UserName,
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                WalletOutcome = new WalletOutcomeWithTransactionsDto(_walletService.ConvertToWalletOutcome(wallet))
                {
                    Transactions = PagedList<TransactionDto>.ToPagedList(_transactionService.ConvertTransactions(wallet.Transactions),
                        request.TransactionParameters.PageNumber, request.TransactionParameters.PageSize)
                }
            };
        }
    }
}