﻿using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Models;
namespace FinancialAccounting.Application.Queries
{
    public class GetTransactionsResponse : BaseRequest
    {
        public PagedList<TransactionWithWalletDto> Transactions { get; set; }
    }
}
