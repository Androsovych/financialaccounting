﻿using FinancialAccounting.Application.Models;
using MediatR;
using System.Collections.Generic;

namespace FinancialAccounting.Application.Queries
{
    public class GetTransactionsQuery : BaseRequest, IRequest<GetTransactionsResponse>
    {
        public TransactionParameters TransactionParameters { get; set; }
    }
}