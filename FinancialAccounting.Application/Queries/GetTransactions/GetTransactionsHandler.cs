﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries
{
    public class GetTransactionsHandler : IRequestHandler<GetTransactionsQuery, GetTransactionsResponse>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionService;

        public GetTransactionsHandler(IUnitOfWork unitOfWork, ITransactionService transactionsService)
        {
            _unitOfWork = unitOfWork;
            _transactionService = transactionsService;
        }

        public async Task<GetTransactionsResponse> Handle(GetTransactionsQuery request, CancellationToken cancellationToken)
        {
            var transactions = await _unitOfWork.Transactions.GetWithWalletsAndCategoriesByUsername(request.UserName);

            if (transactions == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

            return new GetTransactionsResponse
            {
                UserName = request.UserName,
                Transactions = PagedList<TransactionWithWalletDto>.ToPagedList(_transactionService.ConvertToTransactionsWithWallets(transactions),
                        request.TransactionParameters.PageNumber, request.TransactionParameters.PageSize)
            };
        }
    }
}