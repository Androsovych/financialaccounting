﻿using FinancialAccounting.Application.Models;

namespace FinancialAccounting.Application.Queries.GetWalletOutcome
{
    public class GetWalletOutcomeResponse : BaseRequest
    {
        public WalletOutcomeWithTransactionsDto WalletOutcome { get; set; }
    }
}