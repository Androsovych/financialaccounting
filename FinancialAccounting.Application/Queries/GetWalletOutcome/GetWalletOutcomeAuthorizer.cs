﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;

namespace FinancialAccounting.Application.Queries
{
    public class GetWalletOutcomeAuthorizer : AbstractRequestAuthorizer<GetWalletOutcomeQuery>
    {
        public override void BuildPolicy(GetWalletOutcomeQuery request)
        {
            UseRequirement(new MustOwnWalletRequirement
            {
                WalletId = request.WalletId,
                UserName = request.UserName
            });
        }
    }
}