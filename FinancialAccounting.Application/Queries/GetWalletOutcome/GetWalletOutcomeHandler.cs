﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Queries.GetWalletOutcome;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries
{
    public class GetWalletOutcomeHandler : IRequestHandler<GetWalletOutcomeQuery, GetWalletOutcomeResponse>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionService;
        IWalletService _walletService;

        public GetWalletOutcomeHandler(IUnitOfWork unitOfWork, ITransactionService transactionService,
            IWalletService walletService)
        {
            _unitOfWork = unitOfWork;
            _transactionService = transactionService;
            _walletService = walletService;
        }

        public async Task<GetWalletOutcomeResponse> Handle(GetWalletOutcomeQuery request, CancellationToken cancellationToken)
        {
            var wallet = await _unitOfWork.Wallets.GetWithTransactionsAsync(request.WalletId);

            if (wallet == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

            return new GetWalletOutcomeResponse
            {
                UserName = request.UserName,
                WalletOutcome = new WalletOutcomeWithTransactionsDto(_walletService.ConvertToWalletOutcome(wallet))
                {
                    Transactions = PagedList<TransactionDto>.ToPagedList(_transactionService.ConvertTransactions(wallet.Transactions),
                        request.TransactionParameters.PageNumber, request.TransactionParameters.PageSize)
                }
            };
        }
    }
}