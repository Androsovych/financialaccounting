﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Queries.GetWalletOutcome;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Queries
{
    public class GetWalletOutcomeQuery : BaseRequest, IRequest<GetWalletOutcomeResponse>
    {
        public int WalletId { get; set; }
        public TransactionParameters TransactionParameters { get; set; }
    }
}
