﻿using FinancialAccounting.Application.Models;
using MediatR;
using System.Collections.Generic;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionListCommand : BaseRequest, IRequest<List<TransactionDto>>
    {
        public List<TransactionDto> Transactions { get; set; } = new List<TransactionDto>();
    }
}
