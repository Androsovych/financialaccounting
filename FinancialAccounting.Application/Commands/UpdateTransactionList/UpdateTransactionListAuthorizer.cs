﻿using FinancialAccounting.Application.Authorization.Requirements;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionListAuthorizer : AbstractRequestAuthorizer<UpdateTransactionListCommand>
    {
        public override void BuildPolicy(UpdateTransactionListCommand request)
        {
            UseRequirement(new MustOwnTransactionsAndWalletsRequirement
            {
                Transactions = request.Transactions,
                UserName = request.UserName
            });
        }
    }
}
