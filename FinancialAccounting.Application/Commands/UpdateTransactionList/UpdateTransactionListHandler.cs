﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionListHandler : IRequestHandler<UpdateTransactionListCommand, List<TransactionDto>>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionsService;

        public UpdateTransactionListHandler(IUnitOfWork unitOfWork, ITransactionService transactionsService)
        {
            _unitOfWork = unitOfWork;
            _transactionsService = transactionsService;
        }

        public async Task<List<TransactionDto>> Handle(UpdateTransactionListCommand request, CancellationToken cancellationToken)
        {
            var transactions = await _transactionsService.ConvertTransactions(request.Transactions);

            _unitOfWork.Transactions.UpdateRange(transactions);
            await _unitOfWork.SaveChangesAsync();

            return _transactionsService.ConvertTransactions(transactions);
        }
    }
}