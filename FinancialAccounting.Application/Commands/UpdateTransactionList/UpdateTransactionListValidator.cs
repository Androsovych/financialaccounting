﻿using FinancialAccounting.Application.Models;
using FluentValidation;
using System.Collections.Generic;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionListValidator : AbstractValidator<UpdateTransactionListCommand>
    {
        public UpdateTransactionListValidator()
        {
            RuleFor(x => x.Transactions).Must((l) => l.Count > 0).WithMessage(Resources.Resources.EmptyTransactionList);
            RuleForEach(x => x.Transactions).SetValidator(new TransactionValidator());
            RuleFor(x => x.Transactions).Must(ContainDifferentTransactions).WithMessage(Resources.Resources.SameTransactionsIdError);
        }

        private static bool ContainDifferentTransactions(List<TransactionDto> transactions)
        {
            foreach(var transaction in transactions)
            {
                if (!IsIdUnique(transaction.Id, transactions))
                    return false;
            }

            return true;
        }

        private static bool IsIdUnique(int id, List<TransactionDto> transactions)
        {
            var counter = 0;

            foreach (var transaction in transactions)
            {
                if (transaction.Id == id)
                    counter++;

                if(counter > 1)
                    return false;
            }

            return true;
        }
    }
}
