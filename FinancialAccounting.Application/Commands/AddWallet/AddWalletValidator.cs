﻿using FinancialAccounting.Application.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class AddWalletValidator : AbstractValidator<AddWalletCommand>
    {
        public AddWalletValidator()
        {
            RuleFor(x=>x.Name).Cascade(CascadeMode.Stop).NameLength();
            RuleFor(x => x.Currency).IsInEnum().WithMessage(Resources.Resources.InvalidCurrency);
        }
    }
}