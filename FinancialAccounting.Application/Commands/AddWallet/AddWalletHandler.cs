﻿using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class AddWalletHandler : IRequestHandler<AddWalletCommand, WalletDto>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionsService;

        public AddWalletHandler(IUnitOfWork unitOfWork, ITransactionService transactionsService)
        {
            _unitOfWork = unitOfWork;
            _transactionsService = transactionsService;
        }

        public async Task<WalletDto> Handle(AddWalletCommand request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Users.GetSingleAsync(u => u.UserName == request.UserName);

            var wallet = new Wallet()
            {
                UserId = user.Id,
                Name = request.Name,
                Currency = request.Currency.Value
            };

            await _unitOfWork.Wallets.AddAsync(wallet);
            await _unitOfWork.SaveChangesAsync();

            return new WalletDto(wallet);
        }
    }
}