﻿using FinancialAccounting.Domain.Core;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace FinancialAccounting.Application.Commands
{
    public class AddWalletCommand : BaseRequest, IRequest<WalletDto>
    {
        const string InvalidCurrency = "InvalidCurrency";

        public string Name { get; set; }

        [Required(ErrorMessageResourceName = InvalidCurrency,
            ErrorMessageResourceType = typeof(Resources.Resources))]
        public Currency? Currency { get; set; }
    }
}
