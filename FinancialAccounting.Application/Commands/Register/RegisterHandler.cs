﻿using FinancialAccounting.Application.Commands;
using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using FinancialAccounting.Services.Interfaces;
using MediatR;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Handlers
{
    public class RegisterHandler : IRequestHandler<RegisterCommand, UserDto>
    {
        IUnitOfWork _unitOfWork;
        IJwtGenerator _jwtGenerator;
		IPasswordHasher _passwordHasher;

		public RegisterHandler(IUnitOfWork unitOfWork, IJwtGenerator jwtGenerator, IPasswordHasher passwordHasher)
        {
            _unitOfWork = unitOfWork;
            _jwtGenerator = jwtGenerator;
			_passwordHasher = passwordHasher;
		}

        public async Task<UserDto> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
			if (await _unitOfWork.Users.IsAnyAsync(x => x.Email == request.Email))
				throw new RequestHandlingException(HttpStatusCode.BadRequest, Resources.Resources.EmailExists);

			if (await _unitOfWork.Users.IsAnyAsync(x => x.UserName == request.UserName))
				throw new RequestHandlingException(HttpStatusCode.BadRequest, Resources.Resources.UserNameExists);

			var user = new User
			{
				FirstName = request.FirstName,
				LastName = request.LastName,
				Email = request.Email,
				UserName = request.UserName,
				Password = _passwordHasher.Hash(request.Password)
			};

			await _unitOfWork.Users.AddAsync(user);
			await _unitOfWork.SaveChangesAsync();

			return new UserDto(user)
			{
				Token = _jwtGenerator.CreateToken(user)
			};
		}
	}
}