﻿using FinancialAccounting.Application.Commands;
using FinancialAccounting.Application.Extensions;
using FinancialAccounting.Infrastructure.Data;
using FluentValidation;
using System;

namespace FinancialAccounting.Application.Validators
{
    public class RegisterValidator : AbstractValidator<RegisterCommand>
    {
		const int MinPasswordLength = 6;

		public RegisterValidator()
		{
			RuleFor(x => x.FirstName).Cascade(CascadeMode.Stop).Name();
			RuleFor(x => x.LastName).Cascade(CascadeMode.Stop).Name();
			RuleFor(x => x.UserName).Cascade(CascadeMode.Stop).NameLength();

			RuleFor(x => x.Email).Cascade(CascadeMode.Stop).Required()
				.EmailAddress().WithMessage(Resources.Resources.InvalidEmail);

			RuleFor(x => x.Password).Cascade(CascadeMode.Stop).Required()
				.Must(BeOfValidLength).WithMessage(String.Format(Resources.Resources.InvalidLengthPassword,
					MinPasswordLength, Configuration.MaxPasswordLength))
				.Matches("[A-Z]").WithMessage(Resources.Resources.InvalidPasswordLetters)
				.Matches("[0-9]").WithMessage(Resources.Resources.InvalidPasswordNumber);
		}

		private static bool BeOfValidLength(string password)
		{
			if (password.Length < MinPasswordLength ||
				password.Length > Configuration.MaxPasswordLength)
				return false;

			return true;
		}
	}
}
