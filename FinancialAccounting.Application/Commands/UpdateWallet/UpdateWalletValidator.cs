﻿using FinancialAccounting.Application.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateWalletValidator : AbstractValidator<UpdateWalletCommand>
    {
        public UpdateWalletValidator()
        {
            RuleFor(x => x.Id).IdValue();
            RuleFor(x => x.Name).NameLength();
            RuleFor(x => x.Currency).IsInEnum().WithMessage(Resources.Resources.InvalidCurrency);
        }
    }
}
