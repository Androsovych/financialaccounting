﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateWalletHandler : IRequestHandler<UpdateWalletCommand, WalletDto>
    {
        IUnitOfWork _unitOfWork;

        public UpdateWalletHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<WalletDto> Handle(UpdateWalletCommand request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Users.GetSingleAsync(u => u.UserName == request.UserName);

            var wallet = new Wallet()
            {
                Id = request.Id,
                UserId = user.Id,
                Name = request.Name,
                Currency = request.Currency.Value
            };

            _unitOfWork.Wallets.Update(wallet);
            await _unitOfWork.SaveChangesAsync();

            return new WalletDto(wallet);
        }
    }
}
