﻿using FinancialAccounting.Domain.Core;
using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateWalletCommand : BaseRequest, IRequest<WalletDto>
    {
        const string InvalidCurrency = "InvalidCurrency";

        public int Id { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = InvalidCurrency,
            ErrorMessageResourceType = typeof(Resources.Resources))]
        public Currency? Currency { get; set; }
    }
}
