﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateWalletAuthorizer : AbstractRequestAuthorizer<UpdateWalletCommand>
    {
        public override void BuildPolicy(UpdateWalletCommand request)
        {
            UseRequirement(new MustOwnWalletRequirement
            {
                UserName = request.UserName,
                WalletId = request.Id
            });
        }
    }
}
