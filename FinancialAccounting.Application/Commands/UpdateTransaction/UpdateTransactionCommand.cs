﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionCommand : TransactionDto, IRequest<TransactionDto>
    {
    }
}
