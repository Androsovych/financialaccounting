﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionAuthorizer : AbstractRequestAuthorizer<UpdateTransactionCommand>
    {
        public override void BuildPolicy(UpdateTransactionCommand request)
        {
            UseRequirement(new MustOwnTransactionAndWalletRequirement
            {
                TransactionId = request.Id,
                UserName = request.UserName,
                WalletId = request.WalletId
            });
        }
    }
}
