﻿using FluentValidation;
using FinancialAccounting.Application.Extensions;
using System;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionValidator : AbstractValidator<UpdateTransactionCommand>
    {
        public UpdateTransactionValidator()
        {
            RuleFor(x => x.Id).IdValue();
            RuleFor(x => x).SetValidator(new TransactionValidator());
        }
    }
}
