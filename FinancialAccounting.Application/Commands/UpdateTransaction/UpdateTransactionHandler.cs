﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class UpdateTransactionHandler : IRequestHandler<UpdateTransactionCommand, TransactionDto>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionsService;

        public UpdateTransactionHandler(IUnitOfWork unitOfWork, ITransactionService transactionsService)
        {
            _unitOfWork = unitOfWork;
            _transactionsService = transactionsService;
        }

        public async Task<TransactionDto> Handle(UpdateTransactionCommand request, CancellationToken cancellationToken)
        {
            var transaction = await _transactionsService.ConvertTransaction(request);

            _unitOfWork.Transactions.Update(transaction);
            await _unitOfWork.SaveChangesAsync();

            return new TransactionDto(transaction);
        }
    }
}
