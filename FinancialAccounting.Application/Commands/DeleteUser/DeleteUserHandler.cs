﻿using FinancialAccounting.Application.Commands;
using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Handlers
{
    public class DeleteUserHandler : IRequestHandler<DeleteUserCommand>
    {
        IUnitOfWork _unitOfWork;

        public DeleteUserHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.Users.GetSingleAsync(u=>u.UserName == request.UserName);

            if (user == null)
                throw new RequestHandlingException(HttpStatusCode.NotFound);

            _unitOfWork.Users.Delete(user);
            await _unitOfWork.SaveChangesAsync();

            return await Task.FromResult(Unit.Value);
        }
    }
}
