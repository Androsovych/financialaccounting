﻿using FinancialAccounting.Application.Authorization.Requirements;
using MediatR.Behaviors.Authorization;

namespace FinancialAccounting.Application.Commands
{
    public class AddTransactionListAuthorizer : AbstractRequestAuthorizer<AddTransactionListCommand>
    {
        public override void BuildPolicy(AddTransactionListCommand request)
        {
            UseRequirement(new MustOwnWalletsRequirement
            {
                Transactions = request.Transactions,
                UserName = request.UserName
            });
        }
    }
}