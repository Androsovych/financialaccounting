﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Application.Services.Interfaces;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class AddTransactionListHandler : IRequestHandler<AddTransactionListCommand, List<TransactionDto>>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionsService;
        ICategoryService _categoryService;


        public AddTransactionListHandler(IUnitOfWork unitOfWork,
            ITransactionService transactionsService,
            ICategoryService categoryService)
        {
            _unitOfWork = unitOfWork;
            _transactionsService = transactionsService;
            _categoryService = categoryService;
        }

        public async Task<List<TransactionDto>> Handle(AddTransactionListCommand request, CancellationToken cancellationToken)
        {
            await _categoryService.AddCategories(request.Transactions);

            var transactions = await _transactionsService.ConvertTransactions(request.Transactions);

            await _unitOfWork.Transactions.AddRangeAsync(transactions);
            await _unitOfWork.SaveChangesAsync();

            return _transactionsService.ConvertTransactions(transactions);
        }
    }
}
