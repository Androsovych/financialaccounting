﻿using FinancialAccounting.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class AddTransactionListCommand : BaseRequest, IRequest<List<TransactionDto>>
    {
        public List<TransactionWithoutIdDto> Transactions { get; set; } = new List<TransactionWithoutIdDto>();
    }
}
