﻿using FluentValidation;

namespace FinancialAccounting.Application.Commands
{
    public class AddTransactionListValidator : AbstractValidator<AddTransactionListCommand>
    {
        public AddTransactionListValidator()
        {
            RuleFor(x => x.Transactions).Must((l) => l.Count > 0).WithMessage(Resources.Resources.EmptyTransactionList);
            RuleForEach(x => x.Transactions).SetValidator(new TransactionValidator());
        }
    }
}
