﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application
{
    public class DeleteTransactionCommand : BaseRequest, IRequest
    {
        public int TransactionId { get; set; }
    }
}
