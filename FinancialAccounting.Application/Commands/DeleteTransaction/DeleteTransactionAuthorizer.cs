﻿using MediatR.Behaviors.Authorization;

namespace FinancialAccounting.Application.Authorization
{
    public class DeleteTransactionAuthorizer : AbstractRequestAuthorizer<DeleteTransactionCommand>
    {
        public override void BuildPolicy(DeleteTransactionCommand request)
        {
            UseRequirement(new MustOwnTransactionRequirement
            {
                TransactionId = request.TransactionId,
                UserName = request.UserName
            });
        }
    }
}
