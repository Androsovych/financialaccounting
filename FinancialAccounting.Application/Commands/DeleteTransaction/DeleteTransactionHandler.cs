﻿using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class DeleteTransactionHandler : IRequestHandler<DeleteTransactionCommand>
    {
        IUnitOfWork _unitOfWork;

        public DeleteTransactionHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeleteTransactionCommand request, CancellationToken cancellationToken)
        {
            var transaction = await _unitOfWork.Transactions.GetAsync(request.TransactionId);

            _unitOfWork.Transactions.Delete(transaction);

            await _unitOfWork.SaveChangesAsync();

            return await Task.FromResult(Unit.Value);
        }
    }
}
