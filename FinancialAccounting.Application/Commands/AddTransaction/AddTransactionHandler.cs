﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application
{
    public class AddTransactionHandler : IRequestHandler<AddTransactionCommand, TransactionDto>
    {
        IUnitOfWork _unitOfWork;
        ITransactionService _transactionsService;

        public AddTransactionHandler(IUnitOfWork unitOfWork, ITransactionService transactionsService)
        {
            _unitOfWork = unitOfWork;
            _transactionsService = transactionsService;
        }

        public async Task<TransactionDto> Handle(AddTransactionCommand request, CancellationToken cancellationToken)
        {
            var transaction = await _transactionsService.ConvertTransaction(request);

            await _unitOfWork.Transactions.AddAsync(transaction);
            await _unitOfWork.SaveChangesAsync();

            return new TransactionDto(transaction);
        }
    }
}
