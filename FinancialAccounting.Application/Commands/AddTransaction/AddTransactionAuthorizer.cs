﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;

namespace FinancialAccounting.Application
{
    public class AddTransactionAuthorizer : AbstractRequestAuthorizer<AddTransactionCommand>
    {
        public override void BuildPolicy(AddTransactionCommand request)
        {
            UseRequirement(new MustOwnWalletRequirement
            {
                WalletId = request.WalletId,
                UserName = request.UserName
            });
        }
    }
}