﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using MediatR;
using System;
using System.Text.Json.Serialization;

namespace FinancialAccounting.Application
{
    public class AddTransactionCommand : TransactionWithoutIdDto, IRequest<TransactionDto>
    {
    }
}
