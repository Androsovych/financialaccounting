﻿using FinancialAccounting.Application.Extensions;
using FluentValidation;
using System;

namespace FinancialAccounting.Application
{
    public class AddTransactionValidator : AbstractValidator<AddTransactionCommand>
    {
        public AddTransactionValidator()
        {
            RuleFor(x => x).SetValidator(new TransactionValidator());
        }
    }
}
