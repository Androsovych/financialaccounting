﻿using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class DeleteWalletHandler : IRequestHandler<DeleteWalletCommand>
    {
        IUnitOfWork _unitOfWork;

        public DeleteWalletHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeleteWalletCommand request, CancellationToken cancellationToken)
        {
            var wallet = await _unitOfWork.Wallets.GetAsync(request.WalletId);

            _unitOfWork.Wallets.Delete(wallet);

            await _unitOfWork.SaveChangesAsync();

            return await Task.FromResult(Unit.Value);
        }
    }
}
