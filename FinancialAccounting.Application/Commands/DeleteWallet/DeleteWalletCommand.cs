﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class DeleteWalletCommand : BaseRequest, IRequest
    {
        public int WalletId { get; set; }
    }
}
