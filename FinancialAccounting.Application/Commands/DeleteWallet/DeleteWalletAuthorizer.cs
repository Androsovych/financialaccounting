﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    class DeleteWalletAuthorizer : AbstractRequestAuthorizer<DeleteWalletCommand>
    {
        public override void BuildPolicy(DeleteWalletCommand request)
        {
            UseRequirement(new MustOwnWalletRequirement
            {
                WalletId = request.WalletId,
                UserName = request.UserName
            });
        }
    }
}
