﻿using FinancialAccounting.Application.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class DeletePeriodTransactionsCommand : BaseRequest, IRequest<DeleteTransactionListDto>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int WalletId { get; set; }
    }
}
