﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Commands
{
    public class DeletePeriodTransactionsHandler : IRequestHandler<DeletePeriodTransactionsCommand, DeleteTransactionListDto>
    {
        IUnitOfWork _unitOfWork;

        public DeletePeriodTransactionsHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<DeleteTransactionListDto> Handle(DeletePeriodTransactionsCommand request, CancellationToken cancellationToken)
        {
            Expression<Func<Transaction, bool>> predicate;

            if (request.StartDate.Date == request.EndDate.Date)
                predicate = (t) => t.WalletId == request.WalletId && t.Date.Date == request.StartDate.Date;
            else
                predicate = (t) => t.WalletId == request.WalletId
                    && t.Date.Date >= request.StartDate.Date && t.Date.Date <= request.EndDate.Date;

            _unitOfWork.Transactions.RemoveRange(predicate);

            var result = await _unitOfWork.SaveChangesAsync();

            return new DeleteTransactionListDto
            {
                DeletedTrasactionsNumber = result
            };
        }
    }
}