﻿using FinancialAccounting.Application.Authorization;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Application.Commands
{
    public class DeletePeriodTransactionsAuthorizer : AbstractRequestAuthorizer<DeletePeriodTransactionsCommand>
    {
        public override void BuildPolicy(DeletePeriodTransactionsCommand request)
        {
            UseRequirement(new MustOwnWalletRequirement
            {
                WalletId = request.WalletId,
                UserName = request.UserName
            });
        }
    }
}
