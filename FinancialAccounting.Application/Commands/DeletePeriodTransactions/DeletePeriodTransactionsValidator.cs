﻿using FinancialAccounting.Application.Extensions;
using FluentValidation;
using System;

namespace FinancialAccounting.Application.Commands
{
    public class DeletePeriodTransactionsValidator : AbstractValidator<DeletePeriodTransactionsCommand>
    {
        public DeletePeriodTransactionsValidator()
        {
            RuleFor(x => x.WalletId).IdValue();
            RuleFor(x => x.StartDate).DateValue();
            RuleFor(x => x.EndDate).DateValue();
            RuleFor(x => x).Must((x) => x.StartDate.Date <= x.EndDate.Date)
                .WithMessage(Resources.Resources.InvalidDatePeriod);
        }
    }
}