﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Interfaces;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Authorization.Requirements
{
    public class MustOwnWalletsRequirement : IAuthorizationRequirement
    {
        public string UserName { get; set; }
        public List<TransactionWithoutIdDto> Transactions { get; set; }

        public class MustOwnWalletsRequirementHandler : IAuthorizationHandler<MustOwnWalletsRequirement>
        {
            private readonly IUnitOfWork _unitOfWork;

            public MustOwnWalletsRequirementHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<AuthorizationResult> Handle(MustOwnWalletsRequirement requirement, CancellationToken cancellationToken = default)
            {
                var user = await _unitOfWork.Users.GetWithWalletsAsync(requirement.UserName);

                bool walletBelongsToUser = false;

                foreach (var transaction in requirement.Transactions)
                {
                    foreach (var wallet in user.Wallets)
                    {
                        if (transaction.WalletId == wallet.Id)
                        {
                            walletBelongsToUser = true;
                            break;
                        }
                    }

                    if (!walletBelongsToUser)
                        throw new RequestHandlingException(HttpStatusCode.Forbidden,
                            String.Format(Resources.Resources.InaccessibleWallet, transaction.WalletId));

                    walletBelongsToUser = false;
                }

                return AuthorizationResult.Succeed();
            }
        }
    }
}
