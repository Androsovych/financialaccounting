﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Domain.Interfaces;
using MediatR.Behaviors.Authorization;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Authorization
{
    public class MustOwnWalletRequirement : IAuthorizationRequirement
    {
        public string UserName { get; set; }
        public int WalletId { get; set; }

        public class MustOwnWalletRequirementHandler : IAuthorizationHandler<MustOwnWalletRequirement>
        {
            private readonly IUnitOfWork _unitOfWork;

            public MustOwnWalletRequirementHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<AuthorizationResult> Handle(MustOwnWalletRequirement requirement, CancellationToken cancellationToken = default)
            {
                var isAnyWallet = await _unitOfWork.Wallets.IsAnyAsync(t => t.Id == requirement.WalletId);

                if (!isAnyWallet)
                    throw new RequestHandlingException(HttpStatusCode.NotFound);

                var result = await _unitOfWork.Wallets.IsAnyAsync(requirement.WalletId, requirement.UserName);

                if (!result)
                   throw new RequestHandlingException(HttpStatusCode.Forbidden);

                return AuthorizationResult.Succeed();
            }
        }
    }
}
