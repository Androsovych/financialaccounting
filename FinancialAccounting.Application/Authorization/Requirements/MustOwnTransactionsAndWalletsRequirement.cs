﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Interfaces;
using MediatR.Behaviors.Authorization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Authorization.Requirements
{
    public class MustOwnTransactionsAndWalletsRequirement : IAuthorizationRequirement
    {
        public string UserName { get; set; }
        public List<TransactionDto> Transactions { get; set; }

        public class MustOwnTransactionsAndWalletsRequirementHandler : IAuthorizationHandler<MustOwnTransactionsAndWalletsRequirement>
        {
            private readonly IUnitOfWork _unitOfWork;

            public MustOwnTransactionsAndWalletsRequirementHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<AuthorizationResult> Handle(MustOwnTransactionsAndWalletsRequirement requirement, CancellationToken cancellationToken = default)
            {
                var user = await _unitOfWork.Users.GetWithTransactionsAsync(requirement.UserName);
                bool walletBelongsToUser = false;
                bool transactiontBelongsToUser = false;

                foreach(var transaction in requirement.Transactions)
                {
                    foreach(var userwallet in user.Wallets)
                    {
                        if (transaction.WalletId == userwallet.Id)
                            walletBelongsToUser = true;

                        foreach(var userTransaction in userwallet.Transactions)
                        {
                            if (transaction.Id == userTransaction.Id)
                            {
                                transactiontBelongsToUser = true;
                                break;
                            }
                        }
                    }

                    if (!walletBelongsToUser || !transactiontBelongsToUser)
                        throw new RequestHandlingException(HttpStatusCode.Forbidden,
                            String.Format(Resources.Resources.InvalidTransactionForUpdate,
                            transaction.Id, transaction.WalletId));

                    walletBelongsToUser = false;
                    transactiontBelongsToUser = false;
                }

                return AuthorizationResult.Succeed();
            }
        }
    }
}
