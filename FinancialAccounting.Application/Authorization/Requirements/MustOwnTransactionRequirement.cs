﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Domain.Interfaces;
using MediatR.Behaviors.Authorization;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Authorization
{
    public class MustOwnTransactionRequirement : IAuthorizationRequirement
    {
        public string UserName { get; set; }
        public int TransactionId { get; set; }

        public class MustOwnTransactionRequirementHandler : IAuthorizationHandler<MustOwnTransactionRequirement>
        {
            private readonly IUnitOfWork _unitOfWork;

            public MustOwnTransactionRequirementHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<AuthorizationResult> Handle(MustOwnTransactionRequirement requirement, CancellationToken cancellationToken = default)
            {
                var isAnyTransaction = await _unitOfWork.Transactions.IsAnyAsync(t => t.Id == requirement.TransactionId);

                if (!isAnyTransaction)
                    throw new RequestHandlingException(HttpStatusCode.NotFound);

                var result = await _unitOfWork.Transactions.IsAnyAsync(requirement.TransactionId, requirement.UserName);

                if (!result)
                    throw new RequestHandlingException(HttpStatusCode.Forbidden); ;

                return AuthorizationResult.Succeed();
            }
        }
    }
}
