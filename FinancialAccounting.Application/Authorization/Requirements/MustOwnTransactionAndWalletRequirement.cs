﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Domain.Interfaces;
using MediatR.Behaviors.Authorization;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Authorization
{
    public class MustOwnTransactionAndWalletRequirement : IAuthorizationRequirement
    {
        public string UserName { get; set; }
        public int TransactionId { get; set; }
        public int WalletId { get; set; }

        public class MustOwnTransactionAndWalletRequirementHandler : IAuthorizationHandler<MustOwnTransactionAndWalletRequirement>
        {
            private readonly IUnitOfWork _unitOfWork;

            public MustOwnTransactionAndWalletRequirementHandler(IUnitOfWork unitOfWork)
            {
                _unitOfWork = unitOfWork;
            }

            public async Task<AuthorizationResult> Handle(MustOwnTransactionAndWalletRequirement requirement, CancellationToken cancellationToken = default)
            {
                var isAnyTransaction = await _unitOfWork.Transactions
                    .IsAnyAsync(t=>t.Id == requirement.TransactionId);

                if (!isAnyTransaction)
                    throw new RequestHandlingException(HttpStatusCode.NotFound, Resources.Resources.NotFoundTransaction);

                var isAnyWallet = await _unitOfWork.Wallets
                    .IsAnyAsync(t => t.Id == requirement.WalletId);

                if (!isAnyWallet)
                    throw new RequestHandlingException(HttpStatusCode.NotFound, Resources.Resources.NotFoundWallet);

                isAnyWallet = await _unitOfWork.Wallets
                    .IsAnyAsync(requirement.WalletId, requirement.UserName);

                if (!isAnyWallet)
                    throw new RequestHandlingException(HttpStatusCode.Forbidden,
                        String.Format(Resources.Resources.InaccessibleWallet, requirement.WalletId));

                isAnyTransaction = await _unitOfWork.Transactions
                    .IsAnyAsync(requirement.TransactionId, requirement.UserName);

                if (!isAnyTransaction)
                    throw new RequestHandlingException(HttpStatusCode.Forbidden,
                        String.Format(Resources.Resources.InaccessibleWallet, requirement.TransactionId));

                return AuthorizationResult.Succeed();
            }
        }
    }
}
