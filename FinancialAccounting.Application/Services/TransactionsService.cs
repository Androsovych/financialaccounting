﻿using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using FinancialAccounting.Infrastructure.Business;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Services
{
    public class TransactionsService : ITransactionService
    {
        IUnitOfWork _unitOfWork;

        public TransactionsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Transaction> ConvertTransaction(TransactionDto transaction)
        {
            transaction.ThrowIfNull(nameof(transaction));

            var result = await ConvertTransaction(transaction as TransactionWithoutIdDto);

            result.Id = transaction.Id;

            return result;
        }

        public async Task<Transaction> ConvertTransaction(TransactionWithoutIdDto transaction)
        {
            transaction.ThrowIfNull(nameof(transaction));

            var category = await _unitOfWork.Categories
                    .GetSingleAsync(c => c.Name == transaction.Category);

            var result = new Transaction
            {
                Date = transaction.Date,
                Comment = transaction.Comment,
                Type = transaction.Type.Value,
                Amount = transaction.Amount,
                WalletId = transaction.WalletId
            };

            if(category == null)
                result.Category = new Category() { Name = transaction.Category };
            else
                result.CategoryId = category.Id;

            return result;
        }

        public async Task<List<Transaction>> ConvertTransactions(List<TransactionDto> transactions)
        {
            transactions.ThrowIfNull(nameof(transactions));

            var result = new List<Transaction>();

            foreach (var transaction in transactions)
            {
                result.Add(await ConvertTransaction(transaction));
            }

            return result;
        }

        public async Task<List<Transaction>> ConvertTransactions(List<TransactionWithoutIdDto> transactions)
        {
            transactions.ThrowIfNull(nameof(transactions));

            var result = new List<Transaction>();

            foreach (var transaction in transactions)
            {
                result.Add(await ConvertTransaction(transaction));
            }

            return result;
        }

        public List<TransactionDto> ConvertTransactions(ICollection<Transaction> transactions)
        {
            transactions.ThrowIfNull(nameof(transactions));

            var result = new List<TransactionDto>();

            foreach (var transaction in transactions)
            {
                result.Add(new TransactionDto(transaction));
            }

            return result;
        }

        public List<TransactionWithWalletDto> ConvertToTransactionsWithWallets(ICollection<Transaction> transactions)
        {
            transactions.ThrowIfNull(nameof(transactions));

            var dtoList = new List<TransactionWithWalletDto>();

            foreach (var transaction in transactions)
            {
                dtoList.Add(new TransactionWithWalletDto(transaction));
            }

            return dtoList;
        }
    }
}