﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services.Interfaces;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Services
{
    public class CategoryService : ICategoryService
    {
        IUnitOfWork _unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddCategories(List<TransactionWithoutIdDto> transactions)
        {
            var categories = GetCategories(transactions);

            var newCategories = new List<Category>();

            foreach (var category in categories)
            {
                if (await _unitOfWork.Categories.IsAnyAsync(c => c.Name == category.Name))
                    continue;

                newCategories.Add(category);
            }

            await _unitOfWork.Categories.AddRangeAsync(newCategories);
            await _unitOfWork.SaveChangesAsync();
        }

        private List<Category> GetCategories(List<TransactionWithoutIdDto> transactions)
        {
            var categories = new List<Category>();
            bool isCategoryInList;

            foreach (var transaction in transactions)
            {
                isCategoryInList = false;

                foreach (var category in categories)
                {
                    if (transaction.Category == category.Name)
                    {
                        isCategoryInList = true;
                        break;
                    }
                }

                if (!isCategoryInList)
                    categories.Add(new Category() { Name = transaction.Category });
            }

            return categories;
        }

    }
}
