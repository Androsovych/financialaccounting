﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using System;
using System.Collections.Generic;

namespace FinancialAccounting.Application.Services
{
    public class WalletService : IWalletService
    {
        public WalletOutcomeDto ConvertToWalletOutcome(Wallet wallet)
        {
            if (wallet == null)
                throw new ArgumentNullException(nameof(wallet));

            return CalculateOutcome(wallet); ;
        }

        public List<WalletOutcomeDto> ConvertToWalletOutcomes(ICollection<Wallet> wallets)
        {
            if (wallets == null)
                throw new ArgumentNullException();

            var displayWallets = new List<WalletOutcomeDto>();

            foreach (var wallet in wallets)
            {
                displayWallets.Add(CalculateOutcome(wallet));
            }

            return displayWallets;
        }

        public List<WalletDto> ConvertWallets(ICollection<Wallet> wallets)
        {
            if (wallets == null)
                throw new ArgumentNullException();

            var displayWallets = new List<WalletDto>();

            foreach (var wallet in wallets)
            {
                displayWallets.Add(new WalletDto(wallet));
            }

            return displayWallets;
        }

        private WalletOutcomeDto CalculateOutcome(Wallet wallet)
        {
            var walletDisplay = new WalletOutcomeDto(wallet);

            foreach (var transaction in wallet.Transactions)
            {
                if (transaction.Type == TransactionType.Receipt)
                    walletDisplay.Debit += transaction.Amount;
                else
                    walletDisplay.Credit += transaction.Amount;
            }

            walletDisplay.Total = walletDisplay.Debit - walletDisplay.Credit;

            return walletDisplay;
        }
    }
}