﻿using FinancialAccounting.Application.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Services.Interfaces
{
    public interface ICategoryService
    {
        Task AddCategories(List<TransactionWithoutIdDto> transactions);
    }
}
