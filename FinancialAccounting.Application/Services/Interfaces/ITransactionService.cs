﻿using FinancialAccounting.Application.Helpers;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialAccounting.Application.Services
{
    public interface ITransactionService
    {
        Task<Transaction> ConvertTransaction(TransactionDto transaction);
        Task<Transaction> ConvertTransaction(TransactionWithoutIdDto transaction);
        Task<List<Transaction>> ConvertTransactions(List<TransactionDto> transactions);
        Task<List<Transaction>> ConvertTransactions(List<TransactionWithoutIdDto> transactions);
        List<TransactionDto> ConvertTransactions(ICollection<Transaction> transactions);
        List<TransactionWithWalletDto> ConvertToTransactionsWithWallets(ICollection<Transaction> transactions);
    }
}