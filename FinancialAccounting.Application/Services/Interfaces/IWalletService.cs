﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using System.Collections.Generic;

namespace FinancialAccounting.Application.Services
{
    public interface IWalletService
    {
        List<WalletOutcomeDto> ConvertToWalletOutcomes(ICollection<Wallet> wallets);
        List<WalletDto> ConvertWallets(ICollection<Wallet> wallets);
        WalletOutcomeDto ConvertToWalletOutcome(Wallet wallet);
    }
}