﻿using FinancialAccounting.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FinanceDbContext _context;

        public IUserRepository Users { get; }
        public IWalletRepository Wallets { get; }
        public ITransactionRepository Transactions { get; }
        public ICategoryRepository Categories { get; }

        public UnitOfWork(FinanceDbContext context)
        {
            _context = context;
            Users = new UserRepository(context);
            Wallets = new WalletRepository(context);
            Transactions = new TransactionRepository(context);
            Categories = new CategoryRepository(context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }

}
