﻿using FinancialAccounting.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinancialAccounting.Infrastructure.Data
{
    internal class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.Property(p => p.Id).IsRequired();
            builder.Property(p => p.WalletId).IsRequired();
            builder.Property(p => p.Amount).IsRequired().HasColumnType(Configuration.AmountColumnTypeName); ;
            builder.Property(p => p.Type).IsRequired();
            builder.Property(p => p.Comment).HasMaxLength(Configuration.MaxCommentLength);

            builder.HasData(
                new Transaction
                {
                    Id = 1,
                    WalletId = 1,
                    Date = new System.DateTime(2021, 7, 9),
                    Amount = 10000,
                    CategoryId = 2,
                    Type = TransactionType.Receipt
                },
                new Transaction
                {
                    Id = 2,
                    WalletId = 1,
                    Date = new System.DateTime(2021, 7, 9),
                    Amount = 100.5M,
                    CategoryId = 3,
                    Type = TransactionType.Expense
                },
                new Transaction
                {
                    Id = 3,
                    WalletId = 1,
                    Date = new System.DateTime(2021, 7, 9),
                    Amount = 400.8M,
                    CategoryId = 3,
                    Type = TransactionType.Receipt
                },
                new Transaction
                {
                    Id = 4,
                    WalletId = 1,
                    Date = new System.DateTime(2021, 9, 9),
                    Amount = 1778.5M,
                    CategoryId = 3,
                    Type = TransactionType.Expense
                },
                new Transaction
                {
                    Id = 5,
                    WalletId = 1,
                    Date = new System.DateTime(2021, 9, 10),
                    Amount = 500,
                    CategoryId = 3,
                    Type = TransactionType.Receipt
                },
                new Transaction
                {
                    Id = 6,
                    WalletId = 1,
                    Date = new System.DateTime(2021, 7, 9),
                    Amount = 4498,
                    CategoryId = 3,
                    Type = TransactionType.Expense
                },
                new Transaction
                {
                    Id = 7,
                    WalletId = 2,
                    Date = new System.DateTime(2021, 8, 9),
                    Amount = 30000,
                    CategoryId = 2,
                    Type = TransactionType.Receipt
                },
                new Transaction
                {
                    Id = 8,
                    WalletId = 2,
                    Date = new System.DateTime(2021, 10, 5),
                    Amount = 10000,
                    CategoryId = 3,
                    Type = TransactionType.Expense
                },
                new Transaction
                {
                    Id = 9,
                    WalletId = 3,
                    Date = new System.DateTime(2021, 6, 8),
                    Amount = 38000,
                    CategoryId = 2,
                    Type = TransactionType.Receipt
                });
        }
    }
}