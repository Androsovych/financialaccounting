﻿using FinancialAccounting.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinancialAccounting.Infrastructure.Data
{
    internal class WalletConfiguration : IEntityTypeConfiguration<Wallet>
    {
        public void Configure(EntityTypeBuilder<Wallet> builder)
        {
            builder.Property(e => e.Name).IsRequired().HasMaxLength(Configuration.MaxNameLength);
            builder.Property(e => e.Currency).IsRequired();

            builder.HasData(
                new Wallet
                {
                    Id = 1,
                    Name = "First",
                    UserId = 1,
                    Currency = Currency.UAH
                },
                new Wallet
                {
                    Id = 2,
                    Name = "Second",
                    UserId = 1,
                    Currency = Currency.USD
                },
                new Wallet
                {
                    Id = 3,
                    Name = "General",
                    UserId = 2,
                    Currency = Currency.UAH
                });
        }
    }
}