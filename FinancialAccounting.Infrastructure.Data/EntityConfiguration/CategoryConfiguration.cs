﻿using FinancialAccounting.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinancialAccounting.Infrastructure.Data
{
    internal class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasIndex(e => e.Name).IsUnique();
            builder.Property(p => p.Name).IsRequired().HasMaxLength(Configuration.MaxNameLength);

            builder.HasData(
                new Category
                {
                    Id = 1,
                    Name = "Products"
                },
                new Category
                {
                    Id = 2,
                    Name = "Salary"
                },
                new Category
                {
                    Id = 3,
                    Name = "Other"
                });
        }
    }
}