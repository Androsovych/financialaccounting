﻿using FinancialAccounting.Domain.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinancialAccounting.Infrastructure.Data
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(e => e.UserName).IsUnique();
            builder.HasIndex(e => e.Email).IsUnique();
            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(Configuration.MaxNameLength);
            builder.Property(e => e.LastName).IsRequired().HasMaxLength(Configuration.MaxNameLength);
            builder.Property(e => e.UserName).IsRequired().HasMaxLength(Configuration.MaxNameLength);
            builder.Property(e => e.Email).IsRequired().HasMaxLength(Configuration.MaxEmailLength);
            builder.Property(e => e.Password).IsRequired();

            builder.HasData(
                new User
                {
                    Id = 1,
                    FirstName = "Erin",
                    LastName = "Levy",
                    UserName = "erinlevy",
                    Password = "10000.aFQY0dkqZ8D/vTZAWeBZ6A==.VLYzMxuSui2SXySiomQXSBKkNT9BtVEsiw7nCbOyl1s=",
                    Email = "erinlevy@gmail.com"
                },
                new User
                {
                    Id = 2,
                    FirstName = "Eaton",
                    LastName = "Hogan",
                    UserName = "eatonhogan",
                    Password = "10000.aFQY0dkqZ8D/vTZAWeBZ6A==.VLYzMxuSui2SXySiomQXSBKkNT9BtVEsiw7nCbOyl1s=",
                    Email = "eatonhogan@gmail.com"
                }
            );
        }
    }
}