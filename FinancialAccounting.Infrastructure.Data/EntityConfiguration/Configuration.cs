﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialAccounting.Infrastructure.Data
{
    public class Configuration
    {
        public static readonly int AmountWholePartLength = 13;
        public static readonly int AmountFractionalPartLength = 2;
        public static readonly int MinNameLength = 2;
        public static readonly int MaxNameLength = 50;
        public static readonly int MaxCommentLength = 50;
        public static readonly int MaxPasswordLength = 50;
        public static readonly int MaxEmailLength = 50;

        public static string AmountColumnTypeName
        {
            get
            {
                return String.Format("decimal({0},{1})",
                    AmountWholePartLength, AmountFractionalPartLength);
            }
        }
    }
}
