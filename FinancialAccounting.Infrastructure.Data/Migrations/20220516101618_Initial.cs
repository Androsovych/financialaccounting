﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FinancialAccounting.Infrastructure.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Wallets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Currency = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wallets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Wallets_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WalletId = table.Column<int>(type: "int", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(13,2)", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transactions_Wallets_WalletId",
                        column: x => x.WalletId,
                        principalTable: "Wallets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Products" },
                    { 2, "Salary" },
                    { 3, "Other" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "FirstName", "LastName", "Password", "UserName" },
                values: new object[,]
                {
                    { 1, "erinlevy@gmail.com", "Erin", "Levy", "10000.aFQY0dkqZ8D/vTZAWeBZ6A==.VLYzMxuSui2SXySiomQXSBKkNT9BtVEsiw7nCbOyl1s=", "erinlevy" },
                    { 2, "eatonhogan@gmail.com", "Eaton", "Hogan", "10000.aFQY0dkqZ8D/vTZAWeBZ6A==.VLYzMxuSui2SXySiomQXSBKkNT9BtVEsiw7nCbOyl1s=", "eatonhogan" }
                });

            migrationBuilder.InsertData(
                table: "Wallets",
                columns: new[] { "Id", "Currency", "Name", "UserId" },
                values: new object[] { 1, 0, "First", 1 });

            migrationBuilder.InsertData(
                table: "Wallets",
                columns: new[] { "Id", "Currency", "Name", "UserId" },
                values: new object[] { 2, 1, "Second", 1 });

            migrationBuilder.InsertData(
                table: "Wallets",
                columns: new[] { "Id", "Currency", "Name", "UserId" },
                values: new object[] { 3, 0, "General", 2 });

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[] { "Id", "Amount", "CategoryId", "Comment", "Date", "Type", "WalletId" },
                values: new object[,]
                {
                    { 1, 10000m, 2, null, new DateTime(2021, 7, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 1 },
                    { 2, 100.5m, 3, null, new DateTime(2021, 7, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1 },
                    { 3, 400.8m, 3, null, new DateTime(2021, 7, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 1 },
                    { 4, 1778.5m, 3, null, new DateTime(2021, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1 },
                    { 5, 500m, 3, null, new DateTime(2021, 9, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 1 },
                    { 6, 4498m, 3, null, new DateTime(2021, 7, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1 },
                    { 7, 30000m, 2, null, new DateTime(2021, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 2 },
                    { 8, 10000m, 3, null, new DateTime(2021, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2 },
                    { 9, 38000m, 2, null, new DateTime(2021, 6, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Name",
                table: "Categories",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CategoryId",
                table: "Transactions",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_WalletId",
                table: "Transactions",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserName",
                table: "Users",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Wallets_UserId",
                table: "Wallets",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropTable(
                name: "Wallets");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
