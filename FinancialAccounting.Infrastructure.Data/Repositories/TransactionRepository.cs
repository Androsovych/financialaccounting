﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Infrastructure.Data
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(FinanceDbContext context)
            : base(context)
        {
        }

        public async Task AddRangeAsync(List<Transaction> transactions)
        {
            await _context.Transactions.AddRangeAsync(transactions);
        }

        public async Task<List<Transaction>> GetWithWalletsAndCategoriesByUsername(string userName)
        {
            return await _context.Transactions.AsNoTracking()
                .Include(t => t.Wallet)
                .Include(t => t.Category)
                .Where(t => t.Wallet.User.UserName == userName)
                .ToListAsync();
        }

        public async Task<bool> IsAnyAsync(int id, string userName)
        {
            return await _context.Transactions.Include(t => t.Wallet)
                .ThenInclude(w => w.User)
                .AnyAsync(n => n.Wallet.User.UserName == userName && n.Id == id);
        }

        public void RemoveRange(Expression<Func<Transaction, bool>> predicate)
        {
            _context.Transactions.RemoveRange(_context.Transactions.Where(predicate));
        }

        public void UpdateRange(List<Transaction> transactions)
        {
            _context.Transactions.UpdateRange(transactions);
        }
    }
}
