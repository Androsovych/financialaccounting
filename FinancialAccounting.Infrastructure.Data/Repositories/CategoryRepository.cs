﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Infrastructure.Data
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(FinanceDbContext context)
            : base(context)
        {
        }

        public async Task AddRangeAsync(List<Category> categories)
        {
            await _context.Categories.AddRangeAsync(categories);
        }
    }
}
