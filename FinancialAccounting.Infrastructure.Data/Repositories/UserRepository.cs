﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FinancialAccounting.Infrastructure.Data
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(FinanceDbContext context)
            : base(context)
        {
        }

        public async Task<User> GetWithTransactionsAsync(string userName)
        {
            return await _context.Users
                .AsNoTracking()
                .Include(u => u.Wallets)
                .ThenInclude(w => w.Transactions)
                .SingleOrDefaultAsync(n => n.UserName == userName);
        }

        public async Task<User> GetWithTransactionsAsync(string userName, Expression<Func<Transaction, bool>> predicate)
        {
            return await _context.Users
                .AsNoTracking()
                .Include(u => u.Wallets)
                .ThenInclude(w => w.Transactions
                    .AsQueryable().Where(predicate))
                .SingleOrDefaultAsync(n => n.UserName == userName);
        }

        public async Task<User> GetWithWalletsAsync(string userName)
        {
            return await _context.Users
                .AsNoTracking()
                .Include(u => u.Wallets)
                .SingleOrDefaultAsync(n => n.UserName == userName);
        }
    }
}