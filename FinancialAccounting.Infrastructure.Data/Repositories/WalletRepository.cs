﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FinancialAccounting.Infrastructure.Data
{
    public class WalletRepository : Repository<Wallet>, IWalletRepository
    {
        public WalletRepository(FinanceDbContext context)
            : base(context)
        {
        }

        public async Task<Wallet> GetWithTransactionsAsync(int id)
        {
            return await _context.Wallets
                .AsNoTracking()
                .Include(w => w.Transactions.OrderByDescending(t => t.Date))
                .ThenInclude(t => t.Category)
                .SingleOrDefaultAsync(w => w.Id == id);
        }

        public async Task<Wallet> GetWithTransactionsAsync(int id, Expression<Func<Transaction, bool>> predicate)
        {
            return await _context.Wallets
                .AsNoTracking()
                .Include(w => w.Transactions
                    .AsQueryable().Where(predicate).OrderByDescending(t => t.Date))
                .ThenInclude(t => t.Category)
                .SingleOrDefaultAsync(w => w.Id == id);
        }

        public async Task<bool> IsAnyAsync(int id, string userName)
        {
            return await _context.Wallets.Include(w => w.User)
                .AnyAsync(n => n.User.UserName == userName && n.Id == id);
        }
    }
}