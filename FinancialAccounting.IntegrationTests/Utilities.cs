﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Infrastructure.Business;
using FinancialAccounting.Infrastructure.Data;
using FinancialAccounting.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.IntegrationTests
{
    public static class Utilities
    {
        public const int UserId = 1;
        public const int WalletId = 1;
        public const string UserName = "TestUserName";
        public const string AuthenticationScheme = "Bearer";
        public const string ConfigFile = "appsettings.json";
        public const int SecondUserId = 2;
        public const int SecondWalletId = 2;

        public static void SeedDbForTests(FinanceDbContext db)
        {
            db.Users.AddRange(GetTestUsers());
            db.Wallets.AddRange(GetTestWallets());
            db.SaveChanges();
        }

        public static void ReinitializeDbForTests(FinanceDbContext db)
        {
            db.Users.RemoveRange(db.Users);
            db.Wallets.RemoveRange(db.Wallets);
            db.Transactions.RemoveRange(db.Transactions);
            SeedDbForTests(db);
        }

        public static List<User> GetTestUsers()
        {
            return new List<User>()
            {
                GetUser(UserId),
                GetUser(SecondUserId, "SecondUserName")
            };
        }

        public static List<Wallet> GetTestWallets()
        {
            return new List<Wallet>()
            {
                new Wallet
                {
                    Id = WalletId,
                    UserId = UserId,
                    Name = "Name",
                    Currency = Currency.EUR
                },
                new Wallet
                {
                    Id = SecondWalletId,
                    UserId = SecondUserId,
                    Name = "Name",
                    Currency = Currency.EUR
                }
            };
        }

        public static string GetJwtToken()
        {
            IJwtGenerator jwtGenerator = new JwtGenerator(
                new ConfigurationBuilder().AddJsonFile(ConfigFile).Build());

            return jwtGenerator.CreateToken(GetUser(UserId));
        }

        public static HttpClient AddAuthorization(this HttpClient client)
        {
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(AuthenticationScheme, GetJwtToken());

            return client;
        }

        public static HttpContent GetRequestContent(object content)
        {
            var json = JsonConvert.SerializeObject(content);

            return new StringContent(json, UnicodeEncoding.UTF8, Configuration.MediaType);
        }

        public static async Task<T> Deserialize<T>(HttpResponseMessage response)
        {
            var result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(result);
        }

        static User GetUser(int id, string userName = UserName)
        {
            return new User
            {
                Id = id,
                FirstName = "FirstName",
                LastName = "LastName",
                UserName = userName,
                Password = "Password",
                Email = "email@gmail.com"
            };
        }
    }
}