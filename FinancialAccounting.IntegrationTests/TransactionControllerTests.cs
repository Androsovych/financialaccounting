﻿using FinancialAccounting.Application;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Infrastructure.Business;
using FinancialAccounting.Services.Interfaces;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FinancialAccounting.IntegrationTests
{
    public class TransactionControllerTests : IClassFixture<FinancialAccountingFactory>
    {
        const string PostUrl = "/api/transaction";
        const string JsonSerializationError = "A non-empty request body is required.";

        readonly HttpClient _client;
        protected readonly FinancialAccountingFactory _factory;

        public TransactionControllerTests(FinancialAccountingFactory fixture)
        {
            _factory = fixture;
            _client = _factory.CreateClient();
        }

        [Fact]
        public async Task POST_AddTransaction_RequestWithoutAuthorization_ReturnsUnauthorizedCode()
        {
            var response = await _client.PostAsync(PostUrl, Utilities.GetRequestContent(GetCommand()));

            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task POST_AddTransaction_AddsNewTransaction_ReturnsAddedTransaction()
        {
            _client.AddAuthorization();

            var command = GetCommand();

            var response = await _client.PostAsync(PostUrl, Utilities.GetRequestContent(command));
            var responseContent = JsonConvert.DeserializeObject<TransactionDto>(
                await response.Content.ReadAsStringAsync());

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            Assert.Equal(command.Amount, responseContent.Amount);
            Assert.Equal(command.WalletId, responseContent.WalletId);
            Assert.Equal(command.Type, responseContent.Type);
            Assert.Equal(command.Date, responseContent.Date);
            Assert.Equal(command.Category, responseContent.Category);
            Assert.True(responseContent.Id > 0);
        }

        [Fact]
        public async Task POST_AddTransaction_IfTransactionWithNonExistentWallet_ReturnsNotFoundCode()
        {
            _client.AddAuthorization();

            var command = new AddTransactionCommand
            {
                WalletId = Utilities.WalletId + 99,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Receipt,
                Category = "Other"
            };

            var response = await _client.PostAsync(PostUrl, Utilities.GetRequestContent(command));

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task POST_AddTransaction_IfWalletDoesNotBelongToAuthorizedUser_ReturnsForbiddenCode()
        {
            _client.AddAuthorization();

            var command = new AddTransactionCommand
            {
                WalletId = Utilities.SecondWalletId,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Receipt,
                Category = "Other"
            };

            var response = await _client.PostAsync(PostUrl, Utilities.GetRequestContent(command));

            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }

        [Fact]
        public async Task POST_IfRequestContentIsNull_ReturnsBadRequestCodeWithErrorMessage()
        {
            _client.AddAuthorization();

            var command = GetCommand();

            var response = await _client.PostAsync(PostUrl, Utilities.GetRequestContent((AddTransactionCommand)null));
            var responseContent = JsonConvert.DeserializeObject<ErrorResponse>(
                await response.Content.ReadAsStringAsync());

            Assert.True(responseContent.Errors.Count == 1);
            Assert.Equal(JsonSerializationError, responseContent.Errors[0]);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        private AddTransactionCommand GetCommand()
        {
            return new AddTransactionCommand
            {
                WalletId = Utilities.WalletId,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Receipt,
                Category = "Other"
            };
        }
    }
}
