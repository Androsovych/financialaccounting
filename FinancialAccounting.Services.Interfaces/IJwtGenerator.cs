﻿using System;
using FinancialAccounting.Domain.Core;

namespace FinancialAccounting.Services.Interfaces
{
    public interface IJwtGenerator
    {
        string CreateToken(User user);
    }
}
