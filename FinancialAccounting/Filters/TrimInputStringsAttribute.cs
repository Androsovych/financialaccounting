﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FinancialAccounting
{
    public class TrimInputStringsAttribute : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            foreach (var arg in context.ActionArguments)
            {
                if (arg.Value is string)
                {
                    string val = arg.Value as string;
                    if (!string.IsNullOrEmpty(val))
                        context.ActionArguments[arg.Key] = val.Trim();

                    continue;
                }

                Type argType = arg.Value.GetType();

                if (!argType.IsClass)
                    continue;

                TrimAllStringsInObject(arg.Value, argType);
            }
        }

        private void TrimAllStringsInObject(object arg, Type argType)
        {
            var properties = argType.GetProperties();

            foreach (var property in properties)
            {
                if (property.PropertyType == typeof(string))
                    TrimStringProperty(property, arg);
                else if (IsList(property.GetValue(arg, null)))
                {
                    var values = property.GetValue(arg, null) as IList;

                    foreach(var item in values)
                    {
                        TrimAllStringsInObject(item, item.GetType());
                    }
                }
                else if (property.PropertyType.IsClass)
                    TrimAllStringsInObject(property.GetValue(arg, null), property.PropertyType);
            }
        }

        private void TrimStringProperty(PropertyInfo stringProperty, object arg)
        {
            string currentValue = stringProperty.GetValue(arg, null) as string;

            if (!string.IsNullOrEmpty(currentValue))
                stringProperty.SetValue(arg, currentValue.Trim(), null);
        }

        private bool IsList(object o)
        {
            return o is IList &&
               o.GetType().IsGenericType &&
               o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }
    }
}
