﻿using FinancialAccounting.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FinancialAccounting
{
    public class GetUserNameFilter : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context) { }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var c = context.Controller as BaseController;
            c.UserName = c.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
