﻿using FinancialAccounting.Infrastructure.Business;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace FinancialAccounting
{
    public static class ServiceCollectionExtensions
    {
        const string SwaggerVersion = "Swagger:Version";
        const string SwaggerTitle = "Swagger:Title";
        const string SwaggerSchemeName = "Swagger:SecurityScheme:Name";
        const string SwaggerSchemeHeader = "Swagger:SecurityScheme:Header";
        const string SwaggerSchemeDescription = "Swagger:SecurityScheme:Description";
        const string SwaggerDescription = "Swagger:Description";

        public static IServiceCollection AddJwtBearerAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = configuration[JwtGeneratorConfig.Issuer],
                        ValidAudience = configuration[JwtGeneratorConfig.Audience],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(configuration[JwtGeneratorConfig.Key]))
                    };
                });

            return services;
        }

        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(configuration[SwaggerVersion], new OpenApiInfo
                {
                    Title = configuration[SwaggerTitle],
                    Version = configuration[SwaggerVersion],
                    Description = configuration[SwaggerDescription]
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.SchemaFilter<EnumSchemaFilter>();

                c.AddSecurityDefinition(configuration[SwaggerSchemeName], new OpenApiSecurityScheme
                {
                    Description = configuration[SwaggerSchemeDescription],
                    Name = configuration[SwaggerSchemeHeader],
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                   {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = configuration[SwaggerSchemeName]
                            }
                        },
                        new string[] { }
                    }
                });
            });

            return services;
        }
    }
}
