﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FinancialAccounting
{
    public static class ModelStateExtensions
    {
        const string InvalidDate = "InvalidDate";
        const string InvalidDateFormat = "InvalidDateFormat";

        public static ModelStateDictionary AddDateErrorsAndTryGetDate(this ModelStateDictionary modelState, string stringDate, Configuration config,
            IStringLocalizer<SharedResource> sharedLocalizer, out DateTime date)
        {
            if (DateTime.TryParseExact(stringDate, config.DateFormat,
                config.FormatProvider, config.DateTimeStyle, out date))
            {
                if (date > DateTime.Today)
                    modelState.AddModelError(nameof(date), String.Format(sharedLocalizer[InvalidDate],
                        date.ToString(config.DateFormat)));
            }
            else
            {
                modelState.AddModelError(nameof(date), String.Format(sharedLocalizer[InvalidDateFormat],
                    config.DateFormat));
            }

            return modelState;
        }

        public static List<string> GetErrors(this ModelStateDictionary modelState)
        {
            return modelState.Values.Where(v => v.Errors.Count > 0)
                .SelectMany(v => v.Errors)
                .Select(v => v.ErrorMessage)
                .ToList();
        }

    }
}
