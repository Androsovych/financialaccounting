﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System.Linq;

namespace FinancialAccounting
{
    public static class MvcOptionsExtensions
    {
        public static MvcOptions AddValidationFilter(this MvcOptions opts)
        {
            opts.Filters.Add(new ValidationFilter());

            return opts;
        }
    }
}
