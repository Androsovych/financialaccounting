using FinancialAccounting.Application;
using FinancialAccounting.Application.Validators;
using FinancialAccounting.Infrastructure.Data;
using FinancialAccounting.Middleware;
using FluentValidation.AspNetCore;
using MediatR;
using MediatR.Behaviors.Authorization.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinancialAccounting
{
    public class Startup
    {
        const string ConnectionStringName = "DefaultConnection";
        const string ResourcesPath = "Resources";
        const string SwaggerEndpointUrl = "Swagger:Endpoint:Url";
        const string SwaggerEndpointName = "Swagger:Endpoint:Name";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(LoginHandler).Assembly);
            services.AddMediatorAuthorization(typeof(LoginHandler).Assembly);
            services.AddAuthorizersFromAssembly(typeof(LoginHandler).Assembly);
            services.AddUnitOfWork(Configuration.GetConnectionString(ConnectionStringName));
            services.AddJwtBearerAuthentication(Configuration);
            services.AddApplicationServices(Configuration);
            services.AddScoped<Configuration>();
            services.AddControllers().AddNewtonsoftJson(); ;

            services.AddMvc(options =>
            {
                options.AddValidationFilter();
            })
                .AddFluentValidation(options =>
                {
                    options.RegisterValidatorsFromAssemblyContaining<LoginValidator>();
                });

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddLocalization(options => options.ResourcesPath = ResourcesPath);
            services.AddSwaggerDocumentation(Configuration);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(Configuration[SwaggerEndpointUrl],
                    Configuration[SwaggerEndpointName]);
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
