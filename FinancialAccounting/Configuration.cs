﻿using System;
using System.Globalization;
using Microsoft.Extensions.Configuration;

namespace FinancialAccounting
{
    public class Configuration
    {
        const string DateFormatName = "DateFormat";
        const string MaxDaysSpanName = "MaxDaysSpan";

        IConfiguration _configuration;

        public static readonly string MediaType = "application/json";

        public IFormatProvider FormatProvider = CultureInfo.InvariantCulture;
        public DateTimeStyles DateTimeStyle = DateTimeStyles.None;

        public string DateFormat => _configuration[DateFormatName];
        public int MaxDaysSpan => Int32.Parse(_configuration[MaxDaysSpanName]);

        public Configuration(IConfiguration configuration)
        {
            _configuration = configuration;
        }
    }
}
