﻿using System.ComponentModel.DataAnnotations;

namespace FinancialAccounting
{
    public class ValidateId : ValidationAttribute
    {
        const string InvalidId = "Id must be positive";

        public ValidateId()
        {
            ErrorMessage = InvalidId;
        }

        public override bool IsValid(object value)
        {
            int id = (int)value;

            if(id < 0)
            {
                return false;
            }

            return true;
        }
    }
}
