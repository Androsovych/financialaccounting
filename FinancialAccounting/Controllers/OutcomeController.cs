﻿using FinancialAccounting.Application.Exceptions;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Queries;
using FinancialAccounting.Application.Queries.GetPeriodWalletOutcome;
using FinancialAccounting.Application.Queries.GetWalletOutcome;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Net;
using System.Threading.Tasks;

namespace FinancialAccounting.Controllers
{
    [Authorize]
    [GetUserNameFilter]
    public class OutcomeController : BaseController
    {
        const string InvalidTimePeriod = "InvalidTimePeriod";
        const string GetDateTemplate = "{startDate}";
        const string GetPeriodTemplate = "{startDate}/{endDate}";
        const string GetWalletTemplate = "{walletId:int}";
        const string GetDateWalletTemplate = "{walletId:int}/{startDate:datetime}";
        const string GetPeriodWalletTemplate = "{walletId:int}/{startDate}/{endDate}";

        public OutcomeController(Configuration config, IMediator mediator, IStringLocalizer<SharedResource> sharedLocalizer)
            : base(config, mediator, sharedLocalizer)
        {
        }

        /// <summary>
        /// Gets wallet with an outcome with paged transactions
        /// </summary>
        /// <response code="200">Returns the wallet with outcome and transactions</response>
        /// <response code="401">If the user isn't authorized</response>
        ///  <response code="400">If the query or rout values aren't correct</response>
        [HttpGet(GetWalletTemplate)]
        public async Task<ActionResult<GetWalletOutcomeResponse>> GetAsync([ValidateId] int walletId,
            [FromQuery] TransactionParameters transactionParameters)
        {
            var response = await _mediator.Send(new GetWalletOutcomeQuery
            {
                UserName = UserName,
                WalletId = walletId,
                TransactionParameters = transactionParameters
            });

            AddPaginationValue(response.WalletOutcome.Transactions);

            return response;
        }

        /// <summary>
        /// Gets wallet with an outcome and paged transactions carried out one date or between two dates with paged transactions
        /// </summary>
        /// Іf the start and end dates are the same - the outcome will be for one day.
        /// If there is only one date - the outcome will be from the specified date to today.
        /// The maximum set interval between dates can be 90 days.
        /// <response code="200">Returns the wallet with outcome and paged transactions for specified date or date span</response>
        /// <response code="401">If the user isn't authorized</response>
        ///  <response code="400">If the query or rout values aren't correct</response>
        [HttpGet(GetDateWalletTemplate)]
        [HttpGet(GetPeriodWalletTemplate)]
        public async Task<ActionResult<GetPeriodWalletOutcomeResponse>> GetAsync([ValidateId] int walletId,
            string startDate, string endDate, [FromQuery] TransactionParameters transactionParameters)
        {
            (DateTime, DateTime) dateRange = CheckAndGetDateRange(startDate, endDate);

            var response = await _mediator.Send(new GetPeriodWalletOutcomeQuery
            {
                UserName = UserName,
                WalletId = walletId,
                StartDate = dateRange.Item1,
                EndDate = dateRange.Item2,
                TransactionParameters = transactionParameters
            });

            AddPaginationValue(response.WalletOutcome.Transactions);

            return response;
        }

        /// <summary>
        /// Gets wallets with the outcomes
        /// </summary>
        /// <response code="200">Returns the wallets with outcomes</response>
        /// <response code="401">If the user isn't authorized</response>
        [HttpGet()]
        public async Task<ActionResult<GetOutcomeResponse>> GetAsync()
        {
            return await _mediator.Send(new GetOutcomeQuery()
            {
                UserName = UserName
            });
        }

        /// <summary>
        /// Gets wallets with the outcomes carried out one date or between two dates
        /// </summary>
        /// <remarks>
        /// Date format: dd.MM.yyyy.
        /// Іf the start and end dates are the same - the outcome will be for one day.
        /// If there is only one date - the outcome will be from the specified date to today.
        /// The maximum set interval between dates can be 90 days.
        /// </remarks>
        /// <response code="200">Returns the wallets with outcomes for transactions for specified date or date span</response>
        /// <response code="400">If the dates aren't in correct format</response>
        /// <response code="401">If the user isn't authorized</response>
        [HttpGet(GetPeriodTemplate)]
        [HttpGet(GetDateTemplate)]
        public async Task<ActionResult<GetPeriodOutcomeResponse>> GetAsync(string startDate, string endDate)
        {
            (DateTime, DateTime) dateRange = CheckAndGetDateRange(startDate, endDate);

            return await _mediator.Send(new GetPeriodOutcomeQuery()
            {
                StartDate = dateRange.Item1,
                EndDate = dateRange.Item2,
                UserName = UserName
            });
        }

        private (DateTime, DateTime) CheckAndGetDateRange(string startDate, string endDate)
        {
            (DateTime, DateTime) dateRange;

            ModelState.AddDateErrorsAndTryGetDate(startDate, _config, _sharedLocalizer, out dateRange.Item1);

            if (endDate != null)
                ModelState.AddDateErrorsAndTryGetDate(endDate, _config, _sharedLocalizer, out dateRange.Item2);
            else
                dateRange.Item2 = DateTime.Today;

            if (ModelState.IsValid && (dateRange.Item1 > dateRange.Item2
                || (dateRange.Item2 - dateRange.Item1).Days > _config.MaxDaysSpan))
                ModelState.AddModelError(nameof(startDate) + nameof(endDate),
                    String.Format(_sharedLocalizer[InvalidTimePeriod], _config.MaxDaysSpan));

            if (!ModelState.IsValid)
                throw new RequestHandlingException(HttpStatusCode.BadRequest,
                    ModelState.GetErrors());

            return dateRange;
        }
    }
}