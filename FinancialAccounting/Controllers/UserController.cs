﻿using FinancialAccounting.Application;
using FinancialAccounting.Application.Commands;
using FinancialAccounting.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;

namespace FinancialAccounting.Controllers
{
    public class UserController : BaseController
    {
        const string LoginTemplate = "login";
        const string RegisterTemplate = "register";

        public UserController(Configuration config, IMediator mediator, IStringLocalizer<SharedResource> sharedLocalizer)
            : base(config, mediator, sharedLocalizer)
        {
        }

        /// <summary>
        /// Authorizes a user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "userName" : "erinlevy",
        ///       "password" : "123"
        ///     }
        /// </remarks>
        /// <response code="200">Returns user data with authorization token</response>
        /// <response code="404">If the user isn't registered</response>
        /// <response code="400">If the request body is null</response>
        [TrimInputStrings]
        [HttpPost(LoginTemplate)]
        public async Task<ActionResult<UserDto>> LoginAsync(LoginQuery request)
        {
            return await _mediator.Send(request);
        }

        /// <summary>
        /// Registers user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "firstName" : "Andrew",
        ///       "lastName" : "Garcia",
        ///       "userName" : "andrew_g",
        ///       "email": "andrewgarcia@gmail.com",
        ///       "password" : "12345Q"
        ///     }
        /// </remarks>
        /// <response code="200">Returns user data with authorization token</response>
        /// <response code="400">If the request body is null or user data isn't valid</response>
        [TrimInputStrings]
        [HttpPost(RegisterTemplate)]
        public async Task<ActionResult<UserDto>> RegisterAsync(RegisterCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// Deletes authorized user
        /// </summary>
        /// <response code="204">If the user has been deleted</response>
        /// <response code="404">If the user doesn't exist</response>
        /// <response code="401">If user isn't authorized</response>
        [Authorize]
        [GetUserNameFilter]
        [HttpDelete()]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> DeleteAsync()
        {
            await _mediator.Send(new DeleteUserCommand
            {
                UserName = UserName
            });

            return NoContent();
        }
    }
}
