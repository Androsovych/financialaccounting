﻿using FinancialAccounting.Application.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Security.Claims;

namespace FinancialAccounting.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class BaseController : ControllerBase
    {
        protected const string IdTemplate = "{id}";
        protected const string PaginationHeaderKey = "X-Pagination";

        protected readonly IMediator _mediator;
        protected readonly Configuration _config;
        protected readonly IStringLocalizer<SharedResource> _sharedLocalizer;

        public string UserName { get; set; }

        public BaseController(Configuration config, IMediator mediator, IStringLocalizer<SharedResource> sharedLocalizer)
        {
            _config = config;
            _mediator = mediator;
            _sharedLocalizer = sharedLocalizer;
        }

        protected void AddPaginationValue<T>(PagedList<T> list)
        {
            var metadata = new
            {
                list.TotalCount,
                list.PageSize,
                list.CurrentPage,
                list.TotalPages,
                list.HasNext,
                list.HasPrevious
            };

            Response.Headers.Add(PaginationHeaderKey, JsonConvert.SerializeObject(metadata));
        }
    }
}
