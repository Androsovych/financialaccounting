﻿using FinancialAccounting.Application;
using FinancialAccounting.Application.Commands;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialAccounting.Controllers
{
    [Authorize]
    [GetUserNameFilter]
    public class WalletController : BaseController
    {
        public WalletController(Configuration config, IMediator mediator, IStringLocalizer<SharedResource> sharedLocalizer)
            : base(config, mediator, sharedLocalizer)
        {
        }

        /// <summary>
        /// Gets list of wallets
        /// </summary>
        /// <response code="200">Returns paged list of wallets</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="400">If the wallets weren't found</response>
        [HttpGet()]
        public async Task<ActionResult<GetWalletsResponse>> Get([FromQuery] WalletParameters walletParameters)
        {
            var response = await _mediator.Send(new GetWalletsQuery()
            {
                UserName = UserName,
                WalletParameters = walletParameters
            });

            AddPaginationValue(response.Wallets);

            return response;
        }

        /// <summary>
        /// Creates a new wallet
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "name": "Main",
        ///       "currency": "UAH"
        ///     }
        /// </remarks>
        /// <response code="200">Returns created wallet</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="400">If the request body is null or wallet data isn't valid</response>
        [TrimInputStrings]
        [HttpPost()]
        public async Task<ActionResult<WalletDto>> Add(AddWalletCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }

        /// <summary>
        /// Deletes a wallet by id
        /// </summary>
        /// <response code="204">If the wallet has been deleted</response>
        /// <response code="400">If the id value isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If the wallet doesn't belong to the user</response>
        /// <response code="404">If the wallet doesn't exist</response>
        [HttpDelete(IdTemplate)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Delete([ValidateId] int id)
        {
            await _mediator.Send(new DeleteWalletCommand()
            {
                UserName = UserName,
                WalletId = id
            });

            return NoContent();
        }

        /// <summary>
        /// Updates the wallet
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "id": 1,
        ///       "name": "Main",
        ///       "currency": "UAH"
        ///     }
        /// </remarks>
        /// <response code="200">Returns updated wallet</response>
        /// <response code="400">If the request body is null or wallet data isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If the wallet doesn't belong to the user</response>
        /// <response code="404">If the wallet doesn't exist</response>
        [TrimInputStrings]
        [HttpPut()]
        public async Task<ActionResult<WalletDto>> Update(UpdateWalletCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }
    }
}