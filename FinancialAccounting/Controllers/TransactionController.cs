﻿using FinancialAccounting.Application;
using FinancialAccounting.Application.Commands;
using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialAccounting.Controllers
{
    [Authorize]
    [GetUserNameFilter]
    public class TransactionController : BaseController
    {
        const string DeletePeriodTemplate = "period";
        const string AddListTemplate = "list";
        const string UpdateListTemplate = "list";

        public TransactionController(Configuration config, IMediator mediator, IStringLocalizer<SharedResource> sharedLocalizer)
            :base(config, mediator, sharedLocalizer)
        {
        }

        /// <summary>
        /// Gets list of transactions
        /// </summary>
        /// <response code="200">Returns the list of transactions</response>
        /// <response code="400">If the transactions weren't found</response>
        /// <response code="401">If the user isn't authorized</response>
        [HttpGet()]
        public async Task<ActionResult<GetTransactionsResponse>> Get([FromQuery] TransactionParameters transactionParameters)
        {
            var response = await _mediator.Send(new GetTransactionsQuery()
            {
                UserName = UserName,
                TransactionParameters = transactionParameters
            });

            AddPaginationValue(response.Transactions);

            return response;
        }

        /// <summary>
        /// Creates a new transaction
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "walletId": 1,
        ///       "date": "2021-10-13",
        ///       "amount": 100.11,
        ///       "type": "Receipt",
        ///       "category": "Other"
        ///     }
        /// </remarks>
        /// <response code="200">Returns created transaction</response>
        /// <response code="400">If the request body is null or transaction data isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If the wallet doesn't belong to the user</response>
        /// <response code="404">If the wallet doesn't exist</response>
        [TrimInputStrings]
        [HttpPost()]
        public async Task<ActionResult<TransactionDto>> Add(AddTransactionCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }

        /// <summary>
        /// Deletes a transaction by id
        /// </summary>
        /// <response code="204">If the transaction has been deleted</response>
        /// <response code="400">If id value isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If the transaction doesn't belong to the user</response>
        /// <response code="404">If the transaction doesn't exist</response>
        [HttpDelete(IdTemplate)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> Delete([ValidateId] int id)
        {
            await _mediator.Send(new DeleteTransactionCommand()
            {
                UserName = UserName,
                TransactionId = id
            });

            return NoContent();
        }

        /// <summary>
        /// Updates the transaction
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "id" : 2,
        ///       "walletId": 1,
        ///       "date": "2021-10-13",
        ///       "amount": 100.11,
        ///       "type": "Receipt",
        ///       "category": "Other"
        ///     }
        /// </remarks>
        /// <response code="200">Returns updated transaction</response>
        /// <response code="400">If the request body is null or transaction data isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If the wallet or transaction doesn't belong to the user</response>
        /// <response code="404">If the wallet or transaction doesn't exist</response>
        [TrimInputStrings]
        [HttpPut()]
        public async Task<ActionResult<TransactionDto>> Update(UpdateTransactionCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }

        /// <summary>
        /// Deletes the transactions of the specified wallet carried out one date or between two dates
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "walletId": 1,
        ///       "startDate": "2021-09-13",
        ///       "endDate": "2021-10-13"
        ///     }
        /// </remarks>
        /// <response code="200">Returns the number of deleted transactions</response>
        /// <response code="400">If the request body is null or request data isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If the wallet doesn't belong to the user</response>
        /// <response code="404">If the wallet doesn't exist</response>
        [HttpDelete(DeletePeriodTemplate)]
        public async Task<ActionResult<DeleteTransactionListDto>> Delete(DeletePeriodTransactionsCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }

        /// <summary>
        /// Adds a list of transactions
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "transactions":[
        ///        {
        ///          "walletId": 1,
        ///          "date": "2021-10-13",
        ///          "amount": 100.11,
        ///          "type": "Receipt",
        ///          "category": "Other"
        ///         },
        ///         {
        ///          "walletId": 2,
        ///          "date": "2021-10-14",
        ///          "amount": 200,
        ///          "type": "Expense",
        ///          "category": "Other"
        ///         }
        ///       ]
        ///     }
        /// </remarks>
        /// <response code="200">Returns created transactions</response>
        /// <response code="400">If the request body is null or transactions data isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If one of the wallets doesn't belong to the user</response>
        [TrimInputStrings]
        [HttpPost(AddListTemplate)]
        public async Task<ActionResult<List<TransactionDto>>> AddList(AddTransactionListCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }

        /// <summary>
        /// Updates the list of transactions
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///       "transactions":[
        ///        {
        ///          "id": 1
        ///          "walletId": 1,
        ///          "date": "2021-10-13",
        ///          "amount": 100.11,
        ///          "type": "Receipt",
        ///          "category": "Other"
        ///         },
        ///         {
        ///          "id": 2
        ///          "walletId": 2,
        ///          "date": "2021-10-14",
        ///          "amount": 200,
        ///          "type": "Expense",
        ///          "category": "Other"
        ///         }
        ///       ]
        ///     }
        /// </remarks>
        /// <response code="200">Returns updated transactions</response>
        /// <response code="400">If the request body is null or transactions data isn't valid</response>
        /// <response code="401">If the user isn't authorized</response>
        /// <response code="403">If one of the wallets or one of the transactions doesn't belong to the user</response>
        [TrimInputStrings]
        [HttpPut(UpdateListTemplate)]
        public async Task<ActionResult<List<TransactionDto>>> UpdateList(UpdateTransactionListCommand command)
        {
            command.UserName = UserName;

            return await _mediator.Send(command);
        }
    }
}
