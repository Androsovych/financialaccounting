﻿using FinancialAccounting.Application.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace FinancialAccounting.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError("Request processing went wrong : {0}", ex.ToString());
                await HandleExceptionAsync(context, ex, _logger);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception ex, ILogger<ExceptionMiddleware> logger)
        {
            context.Response.ContentType = Configuration.MediaType;
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            object errors = null;

            switch (ex)
            {
                case RequestHandlingException e:
                    errors = e.Errors;
                    context.Response.StatusCode = (int)e.Code;
                    break;
                case Exception e:
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            if (errors != null)
            {
                await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                {
                    errors
                }));
            }
        }
    }
}
