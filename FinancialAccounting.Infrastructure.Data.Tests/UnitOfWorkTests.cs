﻿using Moq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FinancialAccounting.Infrastructure.Data.Tests
{
    public class UnitOfWorkTests
    {
        Mock<FinanceDbContext> _mockContext = new Mock<FinanceDbContext>();

        [Fact]
        public async Task SaveChanges_SavesChangesInDatabase()
        {
            var unitOfWork = new UnitOfWork(_mockContext.Object);

            await unitOfWork.SaveChangesAsync();
            _mockContext.Verify(mock => mock.SaveChangesAsync(default(CancellationToken)), Times.Once());
        }

        [Fact]
        public void IsNameUnique_DisposeContext()
        {
            var unitOfWork = new UnitOfWork(_mockContext.Object);

            unitOfWork.Dispose();
            _mockContext.Verify(mock => mock.Dispose(), Times.Once());
        }

    }
}
