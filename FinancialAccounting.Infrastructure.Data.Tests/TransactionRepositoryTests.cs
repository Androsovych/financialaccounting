﻿using Microsoft.EntityFrameworkCore;
using FinancialAccounting.Domain.Core;
using System;
using Xunit;
using System.Threading.Tasks;

namespace FinancialAccounting.Infrastructure.Data.Tests
{
    public class TransactionRepositoryTests
    {
        [Fact]
        public async Task AddAsync_AddsNewTransaction()
        {
            var options = new DbContextOptionsBuilder<FinanceDbContext>()
            .UseInMemoryDatabase(databaseName: "FinanceDatabase")
            .Options;

            using (var context = new FinanceDbContext(options))
            {
                var expected = new Transaction
                {
                    Id = 2,
                    WalletId = 1,
                    Date = new DateTime(2021, 7, 9),
                    Amount = 100.5M,
                    CategoryId = 3,
                    Type = TransactionType.Expense
                };

                TransactionRepository transactionRepository = new TransactionRepository(context);
                await transactionRepository.AddAsync(expected);
                context.SaveChanges();
                var actual = context.Transactions.Find(expected.Id);

                Assert.Equal(expected.Id, actual.Id);
                Assert.Equal(expected.Amount, actual.Amount);
                Assert.Equal(expected.CategoryId, actual.CategoryId);
                Assert.Equal(expected.Date, actual.Date);
                Assert.Equal(expected.WalletId, actual.WalletId);
                Assert.Equal(expected.Type, actual.Type);
            }
        }
    }
}
