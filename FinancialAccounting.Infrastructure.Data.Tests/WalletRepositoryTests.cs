﻿using FinancialAccounting.Domain.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FinancialAccounting.Infrastructure.Data.Tests
{
    public class WalletRepositoryTests
    {
        [Fact]
        public async Task IsAnyAsync_ReturnsTrueIfWalletWithUserWithSpecifiedUserNameExists()
        {
            var options = new DbContextOptionsBuilder<FinanceDbContext>()
            .UseInMemoryDatabase(databaseName: "FinanceDatabaseWallet")
            .Options;

            var userId = 1;
            var userName = "erinlevy";

            using (var context = new FinanceDbContext(options))
            {
                var wallet = new Wallet
                {
                    Id = 1,
                    UserId = userId
                };

                var user = new User
                {
                    Id = userId,
                    UserName = userName
                };

                context.Wallets.Add(wallet);
                context.Users.Add(user);
                context.SaveChanges();

                WalletRepository walletRepository = new WalletRepository(context);
                var actual = await walletRepository.IsAnyAsync(wallet.Id, userName);

                Assert.True(actual);
            }
        }

        [Fact]
        public async Task IsAnyAsync_ReturnsFalseIfThereIsNoWalletWithUserWithSpecifiedUserName()
        {
            var options = new DbContextOptionsBuilder<FinanceDbContext>()
            .UseInMemoryDatabase(databaseName: "FinanceDatabaseWallet2")
            .Options;

            string userName = "erinlevy";

            using (var context = new FinanceDbContext(options))
            {
                var wallet = new Wallet
                {
                    Id = 1,
                    UserId = 1
                };

                context.Wallets.Add(wallet);
                context.SaveChanges();

                WalletRepository walletRepository = new WalletRepository(context);
                var actual = await walletRepository.IsAnyAsync(wallet.Id, userName);

                Assert.False(actual);
            }
        }
    }
}
