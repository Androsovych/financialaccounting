﻿using MediatR;
using Moq;
using System.Threading.Tasks;
using FinancialAccounting.Controllers;
using Microsoft.Extensions.Localization;
using FinancialAccounting.Application;
using FinancialAccounting.Domain.Core;
using Xunit;
using Microsoft.Extensions.Configuration;
using System.Threading;
using FinancialAccounting.Application.Models;

namespace FinancialAccounting.Tests
{
    public class TransactionControllerTests
    {
        Mock<IMediator> _mockMediator = new Mock<IMediator>();
        Mock<IConfiguration> _mockConfig = new Mock<IConfiguration>();
        Mock<IStringLocalizer<SharedResource>> _mockLocalizer = new Mock<IStringLocalizer<SharedResource>>();

        [Fact]
        public async Task AddTransaction_InvokeMediator()
        {
            var controller = new TransactionController(new Configuration(_mockConfig.Object),
                _mockMediator.Object, _mockLocalizer.Object);

            var transaction = GetTransaction();

            var command = new AddTransactionCommand
            {
                WalletId = transaction.Id,
                Date = transaction.Date,
                Amount = transaction.Amount,
                Type = transaction.Type,
                Category = transaction.Category.Name
            };

            var actual = await controller.Add(command);

            _mockMediator.Verify(x => x.Send(command,
                default(CancellationToken)), Times.Once);
        }

        [Fact]
        public async Task AddTransaction_ReturnsCreatedTransaction()
        {
            var controller = new TransactionController(new Configuration(_mockConfig.Object),
                _mockMediator.Object, _mockLocalizer.Object);

            var expectedTransaction = new TransactionDto(GetTransaction());

            _mockMediator.Setup(x => x.Send(It.IsAny<AddTransactionCommand>(),
                default(CancellationToken))).Returns(Task.FromResult(expectedTransaction));

            var actualResult = await controller.Add(new AddTransactionCommand());

            Assert.IsType<TransactionDto>(actualResult.Value);

            Assert.Equal(expectedTransaction.Amount, actualResult.Value.Amount);
            Assert.Equal(expectedTransaction.Id, actualResult.Value.Id);
            Assert.Equal(expectedTransaction.WalletId, actualResult.Value.WalletId);
            Assert.Equal(expectedTransaction.Type, actualResult.Value.Type);
            Assert.Equal(expectedTransaction.Date, actualResult.Value.Date);
            Assert.Equal(expectedTransaction.Category, actualResult.Value.Category);
        }

        private Transaction GetTransaction()
        {
            return new Transaction
            {
                Id = 1,
                WalletId = 2,
                Date = new System.DateTime(2021, 1, 1),
                Amount = 1000M,
                Type = TransactionType.Expense,
                Category = new Category
                {
                    Id = 1,
                    Name = "Other"
                }
            };
        }
    }
}
