# FinancialAccounting
## Description
FinancingAccounting is web-api application created using ASP.NET 6 Core framework for a home financing accounting. The application allows to control incomes and expenses through such operations:
 - add, delete, update wallets for different currencies (USD, EUR, UAH);
 - add, update transactions one by one and by lists;
 - delete transactions by date and by the interval between two dates;
 - get different outcomes of wallets: whole, for a date, for the interval between two dates, for all wallets, for each wallet separately.
 
 ## Documentation
 All endpoints for the API you can find in the [documentation](https://documenter.getpostman.com/view/17485404/Uzs13RxJ).

## System Requirements
- Microsoft Visual Studio Community 2022
- SQL Server 2019

## Tech Stack
ASP.NET Core Web API, SQL Server, Entity Framework, LINQ<br/>
The application was created using CQRS approach with MediatR and FluentValidation

## Running the project
SQL Server is used to store data. SQL Server 2019 must be installed before starting the application. A database is created and filled using automatic migration at the first launch.
####
The operation of the application can be checked using Postman<br/>
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/fa8990c0d1c54b9ce36a?action=collection%2Fimport)