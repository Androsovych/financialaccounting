﻿using FinancialAccounting.Domain.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialAccounting.Domain.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Task AddRangeAsync(List<Category> categories);
    }
}
