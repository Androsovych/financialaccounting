﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinancialAccounting.Domain.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IWalletRepository Wallets { get; }
        ITransactionRepository Transactions { get; }
        ICategoryRepository Categories { get; }
        Task<int> SaveChangesAsync();
    }
}
