﻿using FinancialAccounting.Domain.Core;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FinancialAccounting.Domain.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetWithTransactionsAsync(string userName);
        Task<User> GetWithTransactionsAsync(string userName, Expression<Func<Transaction, bool>> predicate);
        Task<User> GetWithWalletsAsync(string userName);
    }
}