﻿using FinancialAccounting.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FinancialAccounting.Domain.Interfaces
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        Task<bool> IsAnyAsync(int id, string userName);
        Task AddRangeAsync(List<Transaction> transactions);
        Task<List<Transaction>> GetWithWalletsAndCategoriesByUsername(string userName);
        void UpdateRange(List<Transaction> transactions);
        void RemoveRange(Expression<Func<Transaction, bool>> predicate);
    }
}