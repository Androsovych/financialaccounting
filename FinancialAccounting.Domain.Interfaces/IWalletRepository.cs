﻿using FinancialAccounting.Domain.Core;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FinancialAccounting.Domain.Interfaces
{
    public interface IWalletRepository : IRepository<Wallet>
    {
        Task<bool> IsAnyAsync(int id, string userName);
        Task<Wallet> GetWithTransactionsAsync(int id);
        Task<Wallet> GetWithTransactionsAsync(int id, Expression<Func<Transaction, bool>> predicate);
    }
}