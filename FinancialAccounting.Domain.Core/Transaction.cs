﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FinancialAccounting.Domain.Core
{
    public class Transaction : Entity
    {
        public int WalletId { get; set; }
        public Wallet Wallet { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
        public TransactionType Type { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
