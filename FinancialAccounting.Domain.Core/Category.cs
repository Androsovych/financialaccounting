﻿using System.Collections.Generic;
using System.Linq;

namespace FinancialAccounting.Domain.Core
{
    public class Category : Entity
    {
        public string Name { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
        public Category()
        {
            Transactions = new HashSet<Transaction>();
        }
    }
}