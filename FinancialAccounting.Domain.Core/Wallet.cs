﻿using System.Collections.Generic;
using System.Linq;

namespace FinancialAccounting.Domain.Core
{
    public class Wallet : Entity
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public Currency Currency { get; set; }
        public ICollection<Transaction> Transactions { get; set; }

        public Wallet()
        {
            Transactions = new HashSet<Transaction>();
        }
    }
}