﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace FinancialAccounting.Domain.Core
{
    public class StringToEnumConverter : StringEnumConverter
    {
        const bool IgnoreCase = true;

        public override object ReadJson(JsonReader reader, Type objectType,
            object existingValue, JsonSerializer serializer)
        {
            if (string.IsNullOrEmpty(reader.Value?.ToString()))
                return null;

            object parsedEnumValue;

            if (Enum.TryParse(objectType.GenericTypeArguments[0],
                reader.Value.ToString(), IgnoreCase, out parsedEnumValue))
            {
                return parsedEnumValue;
            }
            
            return null;
        }
    }
}
