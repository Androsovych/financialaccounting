﻿
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace FinancialAccounting.Domain.Core
{
    [JsonConverter(typeof(StringToEnumConverter))]
    public enum Currency
    {
        [EnumMember(Value = nameof(Currency.UAH))]
        UAH,
        [EnumMember(Value = nameof(Currency.USD))]
        USD,
        [EnumMember(Value = nameof(Currency.EUR))]
        EUR
    }
}