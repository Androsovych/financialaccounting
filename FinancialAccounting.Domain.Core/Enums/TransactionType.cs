﻿using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace FinancialAccounting.Domain.Core
{
    [JsonConverter(typeof(StringToEnumConverter))]
    public enum TransactionType
    {
        [EnumMember(Value = nameof(TransactionType.Receipt))]
        Receipt,
        [EnumMember(Value = nameof(TransactionType.Expense))]
        Expense
    }
}
