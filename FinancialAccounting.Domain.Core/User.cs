﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace FinancialAccounting.Domain.Core
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
        public ICollection<Wallet> Wallets { get; set; }

        public User()
        {
            Wallets = new HashSet<Wallet>();
        }

    }
}
