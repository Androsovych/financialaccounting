﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FinancialAccounting.Domain.Core
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
