using FinancialAccounting.Domain.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using Xunit;

namespace FinancialAccounting.Infrastructure.Business.Tests
{
    public class JwtGeneratorTests
    {
        const string ConfigFile = "configuration.json";
        const string TestUserName = "TestUserName";

		IConfiguration _config;

        public JwtGeneratorTests()
        {
            _config = new ConfigurationBuilder().AddJsonFile(ConfigFile).Build();
        }

        [Fact]
        public void CreateToken_CreatesAndReturnsValidToken()
        {
            var jwtGenerator = new JwtGenerator(_config);

            var result = jwtGenerator.CreateToken(new User { UserName = TestUserName });

			Assert.True(ValidateToken(result));
        }

		[Fact]
		public void CreateToken_ReturnsTokenWithUserNameInClames()
		{
			var jwtGenerator = new JwtGenerator(_config);

			var token = jwtGenerator.CreateToken(new User { UserName = TestUserName });

			var tokenHandler = new JwtSecurityTokenHandler();
			var securityToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

			var actualClaimValue = securityToken.Claims.First(
				claim => claim.Type == JwtRegisteredClaimNames.NameId).Value;

			Assert.Equal(TestUserName, actualClaimValue);
		}

		private bool ValidateToken(string token)
		{
			var mySecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));

			var tokenHandler = new JwtSecurityTokenHandler();
			try
			{
				tokenHandler.ValidateToken(token, new TokenValidationParameters
				{
					ValidateIssuerSigningKey = true,
					ValidateIssuer = true,
					ValidateAudience = true,
					ValidateLifetime = true,
					ValidIssuer = _config[JwtGeneratorConfig.Issuer],
					ValidAudience = _config[JwtGeneratorConfig.Audience],
					IssuerSigningKey = mySecurityKey
				}, out SecurityToken validatedToken);
			}
			catch
			{
				return false;
			}
			return true;
		}
	}
}
