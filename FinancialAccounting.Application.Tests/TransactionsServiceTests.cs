﻿using FinancialAccounting.Application.Models;
using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FinancialAccounting.Application.Tests
{
    public class TransactionsServiceTests
    {
        Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        [Fact]
        public async Task ConvertTransaction_ConvertDtoToTransaction_IfCategoryExists()
        {
            var expectedTransaction = TransactionHelper.CreateTransaction();

            var transactionDto = new TransactionWithoutIdDto(expectedTransaction);

            _mockUnitOfWork.Setup(m => m.Categories.GetSingleAsync(It.IsAny<Expression<Func<Category, bool>>>()))
                .Returns(Task.FromResult(expectedTransaction.Category));

            var service = new TransactionsService(_mockUnitOfWork.Object);
            var actual = await service.ConvertTransaction(transactionDto);

            Assert.Equal(expectedTransaction.Amount, actual.Amount);
            Assert.Equal(expectedTransaction.WalletId, actual.WalletId);
            Assert.Equal(expectedTransaction.Type, actual.Type);
            Assert.Equal(expectedTransaction.Date, actual.Date);
            Assert.Equal(expectedTransaction.Category.Id, actual.CategoryId);
            Assert.Equal(0, actual.Id);
        }

        [Fact]
        public async Task ConvertTransaction_ConvertDtoToTransaction_IfCategoryDoesNotExist()
        {
            var expectedTransaction = TransactionHelper.CreateTransaction();

            var transactionDto = new TransactionWithoutIdDto(expectedTransaction);

            _mockUnitOfWork.Setup(m => m.Categories.GetSingleAsync(It.IsAny<Expression<Func<Category, bool>>>()))
                .Returns(Task.FromResult((Category)null));

            var service = new TransactionsService(_mockUnitOfWork.Object);
            var actual = await service.ConvertTransaction(transactionDto);

            Assert.Equal(expectedTransaction.Amount, actual.Amount);
            Assert.Equal(expectedTransaction.WalletId, actual.WalletId);
            Assert.Equal(expectedTransaction.Type, actual.Type);
            Assert.Equal(expectedTransaction.Date, actual.Date);
            Assert.Equal(expectedTransaction.Category.Name, actual.Category.Name);
            Assert.Equal(0, actual.Category.Id);
            Assert.Equal(0, actual.Id);
        }
    }
}
