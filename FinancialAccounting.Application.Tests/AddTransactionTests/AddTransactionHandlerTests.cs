﻿using FinancialAccounting.Application.Services;
using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using Moq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace FinancialAccounting.Application.Tests
{
    public class AddTransactionHandlerTests
    {
        Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();
        Mock<ITransactionService> _mockTransactionsService = new Mock<ITransactionService>();

        [Fact]
        public async Task Handle_ReturnsTransactionDtoOfCreatedTransaction()
        {
            var expectedTransaction = TransactionHelper.CreateTransaction();

            var command = new AddTransactionCommand
            {
                WalletId = expectedTransaction.WalletId,
                Date = expectedTransaction.Date,
                Amount = expectedTransaction.Amount,
                Type = expectedTransaction.Type,
                Category = expectedTransaction.Category.Name
            };

            _mockTransactionsService.Setup(m => m.ConvertTransaction(command))
                .Returns(Task.FromResult(expectedTransaction));

            _mockUnitOfWork.Setup(m => m.Transactions.AddAsync(It.IsAny<Transaction>()))
                .Returns(Task.CompletedTask);

            var handler = new AddTransactionHandler(_mockUnitOfWork.Object, _mockTransactionsService.Object);
            var result = await handler.Handle(command, default(CancellationToken));

            Assert.Equal(expectedTransaction.Id, result.Id);
            Assert.Equal(expectedTransaction.WalletId, result.WalletId);
            Assert.Equal(expectedTransaction.Date, result.Date);
            Assert.Equal(expectedTransaction.Amount, result.Amount);
            Assert.Equal(expectedTransaction.Type, result.Type);
            Assert.Equal(expectedTransaction.Category.Name, result.Category);
        }

        [Fact]
        public async Task Handle_ConvertsRequestAndAddsTransaction()
        {
            var transaction = TransactionHelper.CreateTransaction();

            _mockTransactionsService.Setup(m => m.ConvertTransaction(It.IsAny<AddTransactionCommand>()))
                .Returns(Task.FromResult(transaction));
            _mockUnitOfWork.Setup(m => m.Transactions.AddAsync(It.IsAny<Transaction>()))
                .Returns(Task.CompletedTask);

            var handler = new AddTransactionHandler(_mockUnitOfWork.Object, _mockTransactionsService.Object);
            var result = await handler.Handle(new AddTransactionCommand(), default(CancellationToken));

            _mockTransactionsService.Verify(x => x.ConvertTransaction(It.IsAny<AddTransactionCommand>()), Times.Once());
            _mockUnitOfWork.Verify(x => x.Transactions.AddAsync(It.IsAny<Transaction>()), Times.Once());
            _mockUnitOfWork.Verify(x => x.SaveChangesAsync(), Times.Once());
        }
    }
}
