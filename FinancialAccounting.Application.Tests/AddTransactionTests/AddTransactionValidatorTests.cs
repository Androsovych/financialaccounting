﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Infrastructure.Data;
using FluentValidation.TestHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FinancialAccounting.Application.Tests
{
    public class AddTransactionValidatorTests
    {
        const string InvalidPositiveProperty = "InvalidPositiveProperty";
        const string PropertyName = "PropertyName";
        const string PropertyNamePlaceHolder = "{PropertyName}";
        const string InvalidDate = "InvalidDate";
        const string InvalidTransactionType = "InvalidTransactionType";
        const string InvalidAmount = "InvalidAmount";
        const string InvalidNameLength = "InvalidNameLength";

        AddTransactionValidator validator = new AddTransactionValidator();
        ResourceManager _resourceManager = new ResourceManager(
           "FinancialAccounting.Application.Resources.Resources", typeof(AddTransactionCommand).Assembly);

        [Fact]
        public void Should_have_error_when_WalletId_equals_or_is_less_than_zero()
        {
            var model = new AddTransactionCommand
            {
                WalletId = 0,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Expense,
                Category = "Other"
            };

            var result = validator.TestValidate(model);
            var validationFalure = result.ShouldHaveValidationErrorFor(command => command.WalletId);

            string errorMessage = _resourceManager.GetString(InvalidPositiveProperty).Replace(PropertyNamePlaceHolder,
                validationFalure.ElementAt(0).FormattedMessagePlaceholderValues[PropertyName].ToString());

            Assert.True(validationFalure.Count() == 1);

            validationFalure.WithErrorMessage(errorMessage);
        }

        [Fact]
        public void Should_have_error_when_Date_is_in_future()
        {
            var model = new AddTransactionCommand
            {
                WalletId = 1,
                Date = DateTime.Today.AddDays(1),
                Amount = 10M,
                Type = TransactionType.Expense,
                Category = "Other"
            };

            var result = validator.TestValidate(model);
            var validationFalure = result.ShouldHaveValidationErrorFor(command => command.Date);

            Assert.True(validationFalure.Count() == 1);

            validationFalure.WithErrorMessage(_resourceManager.GetString(InvalidDate));
        }

        [Fact]
        public void Should_have_error_when_Amount_is_invalid()
        {
            var model = new AddTransactionCommand
            {
                WalletId = 1,
                Date = new DateTime(),
                Amount = 10.111M,
                Type = TransactionType.Expense,
                Category = "Other"
            };

            var result = validator.TestValidate(model);
            var validationFalure = result.ShouldHaveValidationErrorFor(command => command.Amount);

            Assert.True(validationFalure.Count() == 1);

            string errorMessage = String.Format(_resourceManager.GetString(InvalidAmount),
                Configuration.AmountWholePartLength,
                Configuration.AmountFractionalPartLength);

            validationFalure.WithErrorMessage(errorMessage);
        }

        [Fact]
        public void Should_have_error_when_Type_is_invalid()
        {
            var model = new AddTransactionCommand
            {
                WalletId = 1,
                Date = new DateTime(),
                Amount = 10M,
                Type = (TransactionType)77,
                Category = "Other"
            };

            var result = validator.TestValidate(model);
            var validationFalure = result.ShouldHaveValidationErrorFor(command => command.Type);

            Assert.True(validationFalure.Count() == 1);

            validationFalure.WithErrorMessage(_resourceManager.GetString(InvalidTransactionType));
        }

        [Fact]
        public void Should_have_error_when_Category_is_invalid()
        {
            var model = new AddTransactionCommand
            {
                WalletId = 1,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Expense,
                Category = String.Empty
            };

            var result = validator.TestValidate(model);
            var validationFalure = result.ShouldHaveValidationErrorFor(command => command.Category);

            Assert.True(validationFalure.Count() == 1);

            string errorMessage = String.Format(_resourceManager.GetString(InvalidNameLength),
                validationFalure.ElementAt(0).FormattedMessagePlaceholderValues[PropertyName].ToString(),
                Configuration.MinNameLength, Configuration.MaxNameLength);

            validationFalure.WithErrorMessage(errorMessage);
        }

        [Fact]
        public void Should_not_have_error_when_all_properties_are_valid()
        {
            var model = new AddTransactionCommand
            {
                WalletId = 1,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Expense,
                Category = "Other"
            };

            var result = validator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(command => command.WalletId);
            result.ShouldNotHaveValidationErrorFor(command => command.Date);
            result.ShouldNotHaveValidationErrorFor(command => command.Amount);
            result.ShouldNotHaveValidationErrorFor(command => command.Type);
            result.ShouldNotHaveValidationErrorFor(command => command.Category);
        }
    }
}
