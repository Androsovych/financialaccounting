﻿using FinancialAccounting.Application.Authorization;
using FinancialAccounting.Domain.Core;
using System;
using System.Linq;
using Xunit;

namespace FinancialAccounting.Application.Tests
{
    public class AddTransactionAuthorizerTests
    {
        [Fact]
        public void BuildPolicy_MustCreateRequirement()
        {
            var command = new AddTransactionCommand
            {
                UserName = "UserName",
                WalletId = 1,
                Date = new DateTime(),
                Amount = 10M,
                Type = TransactionType.Expense,
                Category = "Other"
            };

            var authorizer = new AddTransactionAuthorizer();
            authorizer.BuildPolicy(command);

            var actualRequirement = (MustOwnWalletRequirement)authorizer.Requirements.ElementAt(0);

            Assert.True(authorizer.Requirements.Count() == 1);
            Assert.Equal(command.WalletId, actualRequirement.WalletId);
            Assert.Equal(command.UserName, actualRequirement.UserName);
        }
    }
}
