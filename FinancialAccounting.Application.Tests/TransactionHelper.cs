﻿using FinancialAccounting.Domain.Core;

namespace FinancialAccounting.Application.Tests
{
    public static class TransactionHelper
    {
        public static Transaction CreateTransaction()
        {
            return new Transaction
            {
                Id = 1,
                WalletId = 2,
                Date = new System.DateTime(2021, 1, 1),
                Amount = 1000M,
                Type = TransactionType.Expense,
                Category = new Category
                {
                    Id = 1,
                    Name = "Other"
                }
            };
        }
    }
}
