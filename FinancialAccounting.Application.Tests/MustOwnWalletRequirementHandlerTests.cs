﻿using FinancialAccounting.Domain.Core;
using FinancialAccounting.Domain.Interfaces;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using static FinancialAccounting.Application.Authorization.MustOwnWalletRequirement;
using FinancialAccounting.Application.Authorization;
using System.Threading;
using System.Linq.Expressions;
using FinancialAccounting.Application.Exceptions;
using System.Net;

namespace FinancialAccounting.Application.Tests
{
    public class MustOwnWalletRequirementHandlerTests
    {
        Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();

        [Fact]
        public async Task Handle_ReturnsSucceedResultIfWalletBelongsToUser()
        {
            _mockUnitOfWork.Setup(u => u.Wallets.IsAnyAsync(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            _mockUnitOfWork.Setup(u => u.Wallets.IsAnyAsync(It.IsAny<Expression<Func<Wallet, bool>>>()))
                .Returns(Task.FromResult(true));

            var handler = new MustOwnWalletRequirementHandler(_mockUnitOfWork.Object);
            var result = await handler.Handle(new MustOwnWalletRequirement(), default(CancellationToken));

            Assert.True(result.IsAuthorized);
        }

        [Fact]
        public async Task Handle_ThrowsRequestHandlingExceptionIfWalletIsNotFound()
        {
            _mockUnitOfWork.Setup(u => u.Wallets.IsAnyAsync(It.IsAny<Expression<Func<Wallet, bool>>>()))
                .Returns(Task.FromResult(false));

            var handler = new MustOwnWalletRequirementHandler(_mockUnitOfWork.Object);

            RequestHandlingException exception = await Assert.ThrowsAsync<RequestHandlingException>(
                () => handler.Handle(new MustOwnWalletRequirement(), default(CancellationToken)));

            Assert.Equal(HttpStatusCode.NotFound, exception.Code);
        }

        [Fact]
        public async Task Handle_ThrowsRequestHandlingExceptionIfWalletDoesNotBelongToUser()
        {
            _mockUnitOfWork.Setup(u => u.Wallets.IsAnyAsync(It.IsAny<Expression<Func<Wallet, bool>>>()))
                .Returns(Task.FromResult(true));

            _mockUnitOfWork.Setup(u => u.Wallets.IsAnyAsync(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));

            var handler = new MustOwnWalletRequirementHandler(_mockUnitOfWork.Object);

            RequestHandlingException exception = await Assert.ThrowsAsync<RequestHandlingException>(
                () => handler.Handle(new MustOwnWalletRequirement(), default(CancellationToken)));

            Assert.Equal(HttpStatusCode.Forbidden, exception.Code);
        }
    }
}
